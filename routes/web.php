<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//google
// Route::get('/auth/google', 'Admin\ManageProductsController@redirectToGoogle');
// Route::get('/auth/google/callback', 'Admin\ManageProductsController@handleGoogleCallback');

Route::get('/', 'PagesController@home');
Route::get('/home', 'PagesController@home');

Route::get('/hot-sales', 'ProductController@hotSales');
Route::get('/about-us', 'PagesController@aboutUs');
Route::get('/promotion', 'PagesController@promotion');
Route::get('/category/{link}', 'PagesController@category');
Route::get('/contact-us', 'PagesController@contactUs');
Route::post('/submit-create-contact-us', 'PagesController@submitContactUs');
Route::get('/policy', 'PagesController@policy');
Route::get('/product/{link}', 'PagesController@products');
Route::post('/submit-buy-product', 'ProductController@submitBuyProduct');

//stripe
Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'AddMoneyController@payWithStripe'));
Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'AddMoneyController@postPaymentWithStripe'));

//cart
Route::get('/cart', 'PagesController@cart');

//search
Route::post('/submit-search-product', 'ProductController@searchProduct');
Route::get('/result-search-product/{key}', 'ProductController@resultSearchProduct');

//newsletter
Route::post('/submit_newsletter', 'NewsletterController@submitNewsletter');

////////admin
//product
Route::get('/admin/product/create', 'Admin\ManageProductsController@createProduct');
Route::post('/submit-create-product-admin', 'Admin\ManageProductsController@submitCreateProduct');
Route::post('/submit-edit-product', 'Admin\ManageProductsController@submitEditProduct');
Route::get('/admin/product/edit/{id}', 'Admin\ManageProductsController@editProduct');
Route::get('/admin/product/delete/{id}', 'Admin\ManageProductsController@deleteProduct');

//category
Route::get('/admin/category/create', 'Admin\ManageCategoriesController@createCategory');
Route::post('/submit-create-category-admin', 'Admin\ManageCategoriesController@submitCreateCategory');
Route::post('/submit-edit-category', 'Admin\ManageCategoriesController@submitEditCategory');
Route::get('/admin/category/edit/{id}', 'Admin\ManageCategoriesController@editCategory');
Route::get('/admin/category/delete/{id}', 'Admin\ManageCategoriesController@deleteCategory');

//collection
Route::get('/admin/collection/create', 'Admin\ManageCollectionsController@createCollection');
Route::post('/submit-create-collection-admin', 'Admin\ManageCollectionsController@submitCreateCollection');
Route::post('/submit-edit-collection', 'Admin\ManageCollectionsController@submitEditCollection');
Route::get('/admin/collection/edit/{id}', 'Admin\ManageCollectionsController@editCollection');
Route::get('/admin/collection/delete/{id}', 'Admin\ManageCollectionsController@deleteCollection');

//video
Route::get('/admin/video/create', 'Admin\ManageVideosController@createVideo');
Route::post('/submit-create-video-admin', 'Admin\ManageVideosController@submitCreateVideo');
Route::post('/submit-edit-video', 'Admin\ManageVideosController@submitEditVideo');
Route::get('/admin/video/edit/{id}', 'Admin\ManageVideosController@editVideo');
Route::get('/admin/video/delete/{id}', 'Admin\ManageVideosController@deleteVideo');

//news
Route::get('/admin/new/create', 'Admin\ManageNewsController@createNews');
Route::post('/submit-create-new-admin', 'Admin\ManageNewsController@submitCreateNews');
Route::post('/submit-edit-new', 'Admin\ManageNewsController@submitEditNews');
Route::get('/admin/new/edit/{id}', 'Admin\ManageNewsController@editNews');
Route::get('/admin/new/delete/{id}', 'Admin\ManageNewsController@deleteNews');

//blogs
Route::get('/admin/blogs/create', 'Admin\ManageBlogsController@createBlogs');
Route::post('/submit-create-blogs-admin', 'Admin\ManageBlogsController@submitCreateBlogs');
Route::post('/submit-edit-blogs', 'Admin\ManageBlogsController@submitEditBlogs');
Route::get('/admin/blogs/edit/{id}', 'Admin\ManageBlogsController@editBlogs');
Route::get('/admin/blogs/delete/{id}', 'Admin\ManageBlogsController@deleteBlogs');

//charge
Route::get('/admin/charge/makeDone/{id}', 'Admin\ChargeController@makeDone');

//contact us
Route::get('/admin/message/delete/{id}', 'Admin\ManageContactUsController@deleteMessage');

//send mail to client
Route::get('/admin/sendmail-subcribe', 'Admin\ManageProductsController@sendMailSubcribe');

//Ajax
Route::post('/store-cart','PagesController@storeCart');
Route::get('/store-cart/{id}','PagesController@storeCartGet');