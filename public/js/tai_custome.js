// zoom-out-product
$(document).ready(function() {
    $('.zoom_mw').elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 750
    });

    $("#zoom_09").elevateZoom({
        gallery: "gallery_09",
        galleryActiveClass: "active"
    });


    //caro
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:5,
                nav:true,
                loop:false
            }
        }
    });

    $('#show_size_tutorial').click(function(){
        $('#tableSizeTutorial').modal('show');
    });
    $('#show_guide_buy').click(function(){
        $('#tableGuideToBuy').modal('show');
    });
    $('#button_click_buy').click(function(){
        $('#modalFormCard').modal('show');
    });
    $('#button_buy_in_cart').click(function(){
        event.preventDefault();
        $('#modalFormCard').modal('show');
    });
    $('#quanity_list').change(function(){
        $('#quanity_of_product').val($(this).val());
    });

    //html editor
    tinymce.init({
        selector: '#tinyMce',
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });

    // $('img'.lazyload({
    //     threshold: 200,
    //     effect: 'FadeIn'
    // }));
});
// end
// click-to-zoom-product
$(".click-menu-show").on("click", function(event) {
    event.preventDefault();
    if ($(this).hasClass('close-m')) {
        $(this).addClass('open-m');
        $(this).removeClass('close-m');
        $(this).parent('.caret-parent').next('ul').find(".item-menu-show").css("display", "block");
    } else {
        $(this).addClass('close-m');
        $(this).removeClass('open-m');
        $(this).parent('.caret-parent').next('ul').find(".item-menu-show").css("display", "none");
    }
});

// $( "#quanity-in-cart" ).change(function() {
//     $('.total-price').html($(this).val() * $('#price-in-cart').val());
// });
$('.cart_list #quanity-in-cart').on('change', function() {
    // alert($('#price-in-cart').val());
    var res = parseInt($(this).val()) * parseInt($('#price-in-cart').val());
    // console.log($(this).parent('.quanity-in-cart-td').siblings('.total-price-td').find('.total-price'));
    // $('.cart_list .total-price').html(res);
    $(this).next('.total-price-hidden').val(res);
    // $(this).parents('.quanity-in-cart-td').siblings('.total-price-td').find('.total-price').html(res.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ' đ');
    $(this).parents('.quanity-in-cart-td').siblings('.total-price-td').find('.total-price').html(res);
});

function delete_item_cart(id){
    var u = $.cookie('list-in-cart').replace(id+',', "");
    u = u.replace(id, "");
    u = u.replace(",,", ",");
    if (u.charAt(0) == ',') {u = u.substring(1);}
    $.cookie("list-in-cart", u, { expires: 1, path: '/' });
    var arr = u.split(",");
    $.cookie("cart-num", arr.length, { expires: 1, path: '/' });
    location.reload();
}

$("#select").change(function(e) {
    var currentValue = $("#select").val();
    smallImage = 'public/images/products-thum/' + currentValue.replace("JPG", "png");
    largeImage = 'public/images/products/' + currentValue;
    $('#gallery_09 a').removeClass('active').eq(currentValue - 1).addClass('active');
    var ez = $('#zoom_09').data('elevateZoom');
    ez.swaptheimage(smallImage, largeImage);
});

function add_cart(id) {
    // $.ajax({
    //     url: $('#base_url').val() + '/store-cart/' + id,
    //     method: "GET",
    //     cache: false,
    //     datatype: 'JSON',
    //     contentType: false,
    //     processData: false,
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     },
    //     // data : { _token: '{!! csrf_token() !!}' , id : menuId }
    //     data : { id : id },
    //     success: function(data)
    //     {
    //         console.log('list: ' + data);
    //         var arr = JSON.parse('['+data+']');
    //         $('.cart-in-menu .num').text(arr.length);
    //         console.log('len: ' + arr.length);
    //     },
    //     error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
    //         console.log(JSON.stringify(jqXHR));
    //         console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
    //     }
    // });
    var string = '';
    if (typeof $.cookie('list-in-cart') == 'undefined'){
        string = $.cookie("list-in-cart", id, { expires: 1, path: '/' });
    } else {
        // string = $.cookie("list-in-cart", $.cookie('list-in-cart') + ',' + id, { expires: 1, path: '/' });
        string = $.cookie('list-in-cart') + ',' + id;
        // Take the following string
        // var string = "spanner, span, spaniel, span";
        var arr = string.split(",");
        var unique = [];
        var list = '';
        $.each(arr, function (index,word) {
            if ($.inArray(word, unique) === -1) 
            {
                unique.push(word);
                if (index == 0) {
                    list = word;
                } else {
                    list = list + ',' + word;
                }
            }
        });
        $.cookie("list-in-cart", list, { expires: 1, path: '/' });
        console.log(unique);
    }
    $.cookie("cart-num", unique.length, { expires: 1, path: '/' });
    // alert($.cookie('list-in-cart'));
    location.reload();
}

function openNav() {
    document.getElementById("nav_fix_top").style.display = "block";
    setTimeout(function(){ document.getElementById("nav_fix_top").style.width = "80%" }, 100);
}

function closeNav() {
    document.getElementById("nav_fix_top").style.width = "0";
    setTimeout(function(){ document.getElementById("nav_fix_top").style.display = "none"; }, 400);
}
// end

/*menu ---------------------------*/
$(document).ready(function() {
    // Remove no-js class
    $('html').removeClass('no-js');

    $('#toggleMenu').on('click', function() {
        if ( $(this).hasClass('js-open') ) {
            $('#nav > ul > li:not(#toggleMenu)').removeClass('js-showElement');
            $(this).removeClass('js-open');
            $(this).attr('aria-expanded', false);
        } else {
            $('#nav > ul > li:not(#toggleMenu)').addClass('js-showElement');
            $(this).addClass('js-open');
            $(this).attr('aria-expanded', true);
        }
        return false; 
    })
    // Add plus mark to li that have a sub menu
    // sub menu
    // ------------------------
    // When interacting with a li that has a sub menu
    $('#nav_fix_top li:has("ul")').on('mouseover keyup click mouseleave', function(e) {
        // If either -
            // tabbing into the li that has a sub menu
            // hovering over the li that has a sub menu
        if ( e.keyCode === 9 | e.type === 'mouseover' ) {
            // Show sub menu
            $(this).children('ul').removeClass('js-hideElement');
            $(this).children('ul').addClass('js-showElement');
        }
        // If mouse leaves li that has sub menu
        if ( e.type === 'mouseleave' ) {
            // hide sub menu
            $(this).children('ul').removeClass('js-showElement');
            $(this).children('ul').addClass('js-hideElement');
        }
        // If clicking on li that has a sub menu
        if ( e.type === 'click' ) {
            // If sub menu is already open
            if ( $(this).children('a').hasClass('js-openSubMenu') ) {
                // remove Open class
                $(this).children('a').removeClass('js-openSubMenu');
                // Hide sub menu
                $(this).children('ul').removeClass('js-showElement');
                $(this).children('ul').addClass('js-hideElement');
            // If sub menu is closed
            } else {
                // add Open class
                $(this).children('a').addClass('js-openSubMenu');
                // Show sub menu
                $(this).children('ul').removeClass('js-hideElement');
                $(this).children('ul').addClass('js-showElement');
            }
        } // end click event
    });

    // Tabbing through Levels of sub menu
    // ------------------------
    // If key is pressed while on the last link in a sub menu
    $('#nav_fix_top li > ul > li:last-child > a').on('keydown', function(e) {
        // If tabbing out of the last link in a sub menu AND not tabbing into another sub menu
        if ( (e.keyCode == 9) && $(this).parent('li').children('ul').length == 0 ) {
                // Close this sub menu
                $(this).parent('li').parent('ul').removeClass('js-showElement');
                $(this).parent('li').parent('ul').addClass('js-hideElement');
            // If tabbing out of a third level sub menu and there are no other links in the parent (level 2) sub menu
            if ( $(this).parent('li').parent('ul').parent('li').parent('ul').parent('li').children('ul').length > 0 
                 && $(this).parent('li').parent('ul').parent('li').is(':last-child') ) {
                    // Close the parent sub menu (level 2) as well
                    $(this).parent('li').parent('ul').parent('li').parent('ul').removeClass('js-showElement');
                    $(this).parent('li').parent('ul').parent('li').parent('ul').addClass('js-hideElement');
            }
        }
    })
})


$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 120) {
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
                $('.s-top').css('display','none');
                $('#fixtopbar').css('top','0');
                $('.navbar-static-top .top-banner-contact .logo-page').css('top','6px');
                $('.navbar-static-top .top-banner-menu .navbar-fixed-top .search-right-clm-mobile').css('top','10px');
                $('#app').css('height','39px');
        } else {
            $('#nav_fix_top').addClass('fixed');
            // $('.before-footer').addClass('lazyload');
        } 
    } else {
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
                $('.s-top').css('display','block');
                $('#fixtopbar').css('top','30px');
                $('.navbar-static-top .top-banner-contact .logo-page').css('top','35px');
                $('.navbar-static-top .top-banner-menu .navbar-fixed-top .search-right-clm-mobile').css('top','38px');
                $('#app').css('height','70px');
        } else {
            $('#nav_fix_top').removeClass('fixed');
            // $('.before-footer').removeClass('lazyload');
        }
    }

    // lazyload

    if (($(window).scrollTop() + $(window).height()) > ($('.line-products').position().top + 40)){
        $('.content-2-column').addClass('lazyload');
    }
    if (($(window).scrollTop() + $(window).height()) > ($('.line-products').position().top + 700)){
        $('.content-3-column').addClass('lazyload');
    }   
    

});