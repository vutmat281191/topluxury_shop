<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categories;

class Categories extends Model
{
    //
    protected $table = 'categories';

    public static function getChildrenCategory($parent_id, $level)
    {
    	return Categories::where('level', $level)->where('parent_id', $parent_id)->get();
    }
}
