<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Newsletter;

class NewsletterController extends Controller
{
	public function __construct()
	{
		$categories = Categories::where('level', 1)->get();
		// $categories = Categories::all();
		view()->share('categories', $categories);
	}

    public function submitNewsletter(Request $request)
	{
		$newsletter = new Newsletter();
		$newsletter->email = $request->input('newsletter');
		$newsletter->save();
		return view('pages.home');
	}
}
