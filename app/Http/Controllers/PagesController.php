<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categories;
use App\Products;
use App\Video;
use App\News;
use App\Collection;
use App\ContactUs;

class PagesController extends Controller
{
	public function __construct()
	{
		$categories = Categories::where('level', 1)->get();
		// $categories = Categories::all();
		view()->share('categories', $categories);
	}

    public function home()
	{
		return view('pages.home');
	}

	public function promotion()
	{
	  	return view('pages.promotion');
	}

	public function cart()
	{
		$arr_cart = [];
		$product_cart = [];
		if (isset($_COOKIE["list-in-cart"]) && !empty($_COOKIE["list-in-cart"])) {
			$list_cart = $_COOKIE["list-in-cart"];
			$arr_cart = explode(",", $list_cart);
			$arr_cart = array_unique($arr_cart);
			$arr_cart = array_filter($arr_cart);
		}
		$product_cart = Products::whereIn('id', $arr_cart)
					// ->join('color', 'users.id', '=', 'contacts.user_id')
					->get();
		return view('pages.cart', ['product_cart' => $product_cart]);
	}

	public function policy()
	{
	  	return view('pages.policy');
	}

	public function aboutUs()
	{
		return view('pages.aboutUs');
	}

	public function contactUs()
	{
		return view('pages.contactUs');
	}

	public function submitContactUs(Request $request)
    {
        $contact = new ContactUs();
        $contact->full_name = $request->input('full_name');
        $contact->email = $request->input('email');
        $contact->phone_number = $request->input('phone_number');
        $contact->message = $request->input('message');
        $contact->save();
        return redirect('/');
    }
	
    public function storeCartGet($id) {
    	$this->setCookieProduct("list-in-cart", $id);
    	// return $_COOKIE["list-in-cart"];
		$arr_viewed = explode(",", $_COOKIE["list-in-cart"]);
		$arr_viewed = array_unique($arr_viewed);
		$arr_viewed = array_filter($arr_viewed);
		$this->setCookieProduct("cart-num", count($arr_viewed));
		$String_viewed = implode(",", $arr_viewed);
		// setcookie("list-in-cart", $String_viewed , time()+36000);
		return $String_viewed;
    }

	public function setCookieProduct($name, $val)
    {
        if (isset($_COOKIE[$name])) {
        	setcookie($name, $_COOKIE[$name] . ',' . $val, time()+36000);
        } else setcookie($name, $val , time()+36000);
    }

	public function products($link)
	{
		$product = Products::where('link', $link)->first();
		$this->setCookieProduct("list-product", $link);
		$arr_viewed = [];
		if (isset($_COOKIE["list-product"])) {
			$list_viewed = $_COOKIE["list-product"];
			$arr_viewed = explode(",", $list_viewed);
			$arr_viewed = array_unique($arr_viewed);
			$arr_viewed = array_filter($arr_viewed);
		}
		$product_viewed = Products::whereIn('link', $arr_viewed)->get();
		return view('pages.product', ['product' => $product, 'product_viewed' => $product_viewed]);
	}

	public function getIdCategory($Obj)
	{
		return $Obj->id;	
	}

	public function category($link)
	{
	  	//list category relate
	  	$list_cate = [];
	  	$cate = Categories::where('link', $link)->first();
	  	
	  	$template = $cate->template;
	  	
	  	if ($template == 'video') {
	  		$video = Video::all();
	  	 	return view('pages.category', ['video' => $video, 'link' => $link, 'template' => $template]);
	  	}
	  	
	  	if ($template == 'collection') {
	  		$collection = Collection::all();
	  	 	return view('pages.category', ['collection' => $collection, 'link' => $link, 'template' => $template]);
	  	}

	  	if ($template == 'new') {
	  		$News = News::all();
	  	 	return view('pages.category', ['News' => $News, 'link' => $link, 'template' => $template]);
	  	}

	  	$list_cate[] = $cate->id;
	  	$level2 = Categories::where('parent_id', $cate->id)->get();
	  	if ($level2->count() > 0) {
	   		foreach ($level2 as $l2) {
	    		$list_cate[] = $l2->id;
	    		$level3 = Categories::where('parent_id', $l2->id)->get();
	    		if ($level3->count() > 0) {
	     			foreach ($level3 as $l3) {
	      				$list_cate[] = $l3->id;
	     			}
	    		}
	   		}
	  	}
	  	// dd($list_cate);exit();
	  	$list_cate = array_unique($list_cate);
	  	$list_cate = array_filter($list_cate);
	  
	  	//list product in list category
	  	$list_products = Products::whereIn('id', $list_cate);

	  	// order
	  	if (isset($_GET["price"])) {
	   		$list_products = $list_products->orderBy('price', $_GET["price"]);
	  	} else {
	   		$list_products = $list_products->orderBy('created_at', 'desc');
	  	}

	  	//filter by color
	  	if (isset($_GET["color"]) && isset(\App\Color::where('variable', $_GET["color"])->first()->id)) {
	   		$id_color = \App\Color::where('variable', $_GET["color"])->first()->id;
	   		$list_products = $list_products->where('color', $id_color);
	  	}

	  	//filter by size
	  	if (isset($_GET["size"]) && isset(\App\Size::where('name', $_GET["size"])->first()->id)) {
	   		$id_size = \App\Size::where('name', $_GET["size"])->first()->id;
	   		$list_products = $list_products->where('name', 'LIKE', "%$id_size%");;
	  	}

	  	//filter grid
	  	$grid = 3;
	  	$class_grid = ' col-sm-4 col-xs-4 ';
	  	if (isset($_GET["grid"])) {
	  		$grid = $_GET["grid"];
	  		if ((int)$grid == 5) {
	  			$class_grid = ' col-sm-3 grid-5 ';
	  		} else {
	  			$class_grid = ' col-sm-4 col-xs-4 ';
	  		}
	  	}

	  	// dd($list_products->get());exit();
	  	return view('pages.category', ['list_products' => $list_products->get(), 'link' => $link, 'class_grid' => $class_grid, 'template' => $cate->template, 'cat_content' => $cate->content]);
	}
}
