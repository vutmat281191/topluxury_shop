<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use Stripe;

class ProductController extends Controller
{
	public function __construct()
	{
		$categories = Categories::where('level', 1)->get();
		// $categories = Categories::all();
		view()->share('categories', $categories);
	}

	public function submitBuyProduct(Request $request)
	{
		Stripe::setApiKey("{{ config('services.stripe.keyho') }}");
		$add = Stripe::create(array(
		  "card" => array(
		    "number" => $request->card_number,
		    "exp_month" => $request->expired_month,
		    "exp_year" => $request->expired_year,
		    "cvc" => $request->cvc
		  )
		));
		dd($add); exit();
	}

	public function urlEncodeSearch($string) {
	    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
	    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
	    return str_replace($entities, $replacements, urlencode($string));
	}

	public function urlDecodeSearch($string) {
	    $entities = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
	    $replacements = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', ' ', ' ', ' ', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
	    return str_replace($entities, $replacements, $string);
	}

	public function searchProduct(Request $request)
	{
		if (!empty($request->input('search'))) {
			return redirect('/result-search-product/' . urlencode($request->input('search')));
		}
		return redirect('/');
	}

	public function resultSearchProduct($key)
	{
		$key2  = urldecode($key);
		$res = Products::where('name', 'LIKE', "%$key2%");
		// order
	  	if (isset($_GET["price"])) {
	   		$res = $res->orderBy('price', $_GET["price"]);
	  	} else {
	   		$res = $res->orderBy('created_at', 'desc');
	  	}
	  	//filter grid
	  	$grid = 3;
	  	$class_grid = ' col-sm-4 col-xs-4 ';
	  	if (isset($_GET["grid"])) {
	  		$grid = $_GET["grid"];
	  		if ((int)$grid == 5) {
	  			$class_grid = ' grid-5 ';
	  		} else {
	  			$class_grid = ' col-sm-4 col-xs-4 ';
	  		}
	  	}

		return view('pages.searchProduct', ['count' => $res->count(), 'list' => $res->get(), 'text_search' => $key2, 'class_grid' => $class_grid]);
	}

	public function hotSales()
	{
		$res = Products::where('name', 'LIKE', "%áo%");
		
		// order
	  	if (isset($_GET["price"])) {
	   		$res = $res->orderBy('price', $_GET["price"]);
	  	} else {
	   		$res = $res->orderBy('created_at', 'desc');
	  	}

	  	//filter grid
	  	$grid = 3;
	  	$class_grid = ' col-sm-4 col-xs-4 ';
	  	if (isset($_GET["grid"])) {
	  		$grid = $_GET["grid"];
	  		if ((int)$grid == 5) {
	  			$class_grid = ' grid-5 ';
	  		} else {
	  			$class_grid = ' col-sm-4 col-xs-4 ';
	  		}
	  	}

		return view('pages.hotSales', ['count' => $res->count(), 'list' => $res->get(), 'class_grid' => $class_grid]);
	}
}
