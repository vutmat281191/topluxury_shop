<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Charge;
use App\Size;
use Image;
use App\Color;
use App\Categories;

class ChargeController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$charges = Charge::all();
		return view('admin.charge.index', ['charges' => $charges]);
	}

	////////////////////////////-----manage Charge-----////////////////////////////
    public function makeDone($id)
    {
        // dd('dfdfdf');exit();
        $charge = Charge::find($id);
        $charge->delivery = 'Done';
        $charge->save();
        return redirect('/admin/charge');
    }
}