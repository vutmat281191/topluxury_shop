<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageVideosController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$video = Video::all();
		return view('admin.managevideos.index', ['video' => $video]);
	}

	////////////////////////////-----manage video-----////////////////////////////
    public function createVideo()
    {
        return view('admin.managevideos.createVideo');
    }

    public function submitCreateVideo(Request $request)
    {
        $video = new Video();
        $video->title = $request->input('title');
        $video->link = $request->input('link');
        $video->save();
        return redirect('/admin/managevideos');
    }

    public function editVideo($id)
    {
        $video = Video::find($id);
        return view('admin.managevideos.editVideo', ['video' => $video]);
    }

    public function submitEditVideo(Request $request)
    {
        $video = Video::find($request->input('id'));
        $video->title = $request->input('title');
        $video->link = $request->input('link');
        $video->save();
        return redirect('/admin/managevideos');
    }

    public function deleteVideo($id)
    {
        // dd('dfdfdf');exit();
        $video = Video::find($id);
        $video->delete();
        return redirect('/admin/managevideos');
    }

}