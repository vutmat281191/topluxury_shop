<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Products;
use App\Size;
use Image;
use App\Color;
use App\Categories;
use App\Newsletter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ManageProductsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$Products = Products::all();
		return view('admin.manageproducts.index', ['Products' => $Products]);
	}

	////////////////////////////-----manage category-----////////////////////////////
    public function createProduct()
    {
        $size = Size::all();
        $color = Color::all();
        $categories = Categories::where('level', 3)->get();
        return view('admin.manageproducts.createProduct', ['categories' => $categories, 'size' => $size, 'color' => $color]);
    }
    public function submitCreateProduct(Request $request)
    {
        $list_image = '';
        if($request->hasFile('image'))
        {
            foreach ($request->file('image') as $key => $photo) {
                $filename = time() . $key;
                $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
                // upload image origin
                Image::make($photo)->save(public_path('/images/products/' . $filenameOri));
                
                // upload image thumbnail
                Image::make($photo)->resize(100, 145)->save(public_path('/images/products-thum/' . $filename . '.png'));
                $list_image = $list_image . $filenameOri . ',';
            }
        }
        // dd();exit();
        $Products = new Products();
        $Products->name = $request->input('name');
        $Products->category = $request->input('category');
        $Products->size = implode(",",$request->input('size'));
        $Products->color = $request->input('color');
        $Products->price = $request->input('price');
        $Products->link = md5(time().$Products->name);
        $Products->describe = $request->input('describe');
        $Products->material = $request->input('material');
        $Products->image = $list_image;
        $Products->thumbnail = '';
        $Products->save();
        return redirect('/admin/manageproducts');
    }

    // public function submitCreateProduct(Request $request)
    // {
    //     $list_image = '';
    //     if($request->hasFile('image'))
    //     {
    //         foreach ($request->file('image') as $key => $photo) {
    //             $filename = time() . $key;
    //             $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
    //             // upload image origin
    //             Image::make($photo)->save(public_path('/images/products/' . $filenameOri));
                
    //             // upload image thumbnail
    //             Image::make($photo)->resize(100, 145)->save(public_path('/images/products-thum/' . $filename . '.png'));
    //             $list_image = $list_image . $filenameOri . ',';
    //         }
    //     }
    //     // dd();exit();
    //     $Products = new Products();
    //     $Products->name = $request->input('name');
    //     $Products->category = $request->input('category');
    //     $Products->size = implode(",",$request->input('size'));
    //     $Products->color = $request->input('color');
    //     $Products->price = $request->input('price');
    //     $Products->link = md5(time().$Products->name);
    //     $Products->describe = $request->input('describe');
    //     $Products->material = $request->input('material');
    //     $Products->image = $list_image;
    //     $Products->thumbnail = '';
    //     $Products->save();
    //     return redirect('/admin/manageproducts');
    // }

    public function editProduct($id)
    {
        $Products = Products::find($id);
        return view('admin.manageproducts.editProduct', ['Products' => $Products]);
    }

    public function submitEditProduct(Request $request)
    {
        $Products = Products::find($request->input('id_category'));
        $Products->title = $request->input('title');
        $Products->content = $request->input('content');
        $Products->save();
        return redirect('/admin/manageproducts');
    }

    public function deleteProduct($id)
    {
        // dd('dfdfdf');exit();
        $Products = Products::find($id);
        $Products->delete();
        return redirect('/admin/manageproducts');
    }

    // public function redirectToGoogle()
    // {
    //     return Socialite::driver('google')->redirect();
    // }

    // public function handleGoogleCallback()
    // {
    //     $user = Socialite::driver('google')->user();
    //     $res = User::where('email', $user->email)->first();
    //     $newsletter = Newsletter::get(['email'])->toArray();
    //     $newsletter2 = array_map(function($a) {  return array_pop($a); }, $newsletter);
    //     $name = 'abc';
    //     try {
    //         foreach ($newsletter2 as $key => $letter) {
    //             $mail = \Mail::send(
    //                 'users.mails.welcomeByGoogle',
    //                 [ 'firstname'=> $name ],
    //                 function($message) use ($email){
    //                     $message->to($email)->subject('Welcome to the viuocmo.com!');
    //                 }
    //             );
    //         }
    //     } catch (Exception $e) {
    //         dd($e);
    //         exit();
    //         return redirect('/');
    //     }
    // }

    public function sendMailSubcribe()
    {
        // echo Hash::make('Abc123!@#');exit();
        if (!Auth::user()){
            return redirect('/login');
        }
        $newsletter = Newsletter::get(['email'])->toArray();
        $newsletter2 = array_map(function($a) {  return array_pop($a); }, $newsletter);
        $name = 'Client';
        foreach ($newsletter2 as $key => $email) {
            // dd($email);exit();
            $mail = \Mail::send(
                'users.mails.subcribe',
                [ 'firstname'=> $name ],
                function($message) use ($email){
                    $message->from('admin@topluxury.com.au', 'Admin')->to($email)->subject('Welcome to the topluxury.com.au!');
                }
            );
        }
    }
}