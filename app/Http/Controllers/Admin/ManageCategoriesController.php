<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Categories;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageCategoriesController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$categories = Categories::all();
    	// dd($categories);exit();
		return view('admin.managecategories.index', ['categories' => $categories]);
	}

	////////////////////////////-----manage category-----////////////////////////////
    public function createCategory()
    {
        $categories = Categories::whereIn('level', [1,2])->get();
        // dd($categories);exit();
        return view('admin.managecategories.createCategory', ['categories' => $categories]);
    }

    public function submitCreateCategory(Request $request)
    {
        // dd($request); exit();
        $category = new Categories();
        $category->name = $request->input('name');
        $category->parent_id = !empty($request->input('parent_id')) ? $request->input('parent_id') : 0;
        $category->level = $request->input('level');
        $category->content = $request->input('content');
        $category->link = md5(time());
        $category->save();
        return redirect('/admin/managecategories');
    }

    public function editCategory($id)
    {
        $categories = Categories::whereIn('level', [1,2])->get();
        $category = Categories::find($id);
        return view('admin.managecategories.editCategory', ['category' => $category, 'categories' => $categories]);
    }

    public function submitEditCategory(Request $request)
    {
        $category = Categories::find($request->input('id_category'));
        $category->name = $request->input('name');
        $category->level = $request->input('level');
        $category->content = $request->input('content');
        $category->save();
        return redirect('/admin/managecategories');
    }

    public function deleteCategory($id)
    {
        // dd('dfdfdf');exit();
        $category = Categories::find($id);
        $category->delete();
        return redirect('/admin/managecategories');
    }
}