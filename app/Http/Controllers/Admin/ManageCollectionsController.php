<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Collection;
use Image;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageCollectionsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$collection = Collection::all();
		return view('admin.managecollections.index', ['collection' => $collection]);
	}

	////////////////////////////-----manage collection-----////////////////////////////
    public function createCollection()
    {
        return view('admin.managecollections.createCollection');
    }

    public function submitCreateCollection(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = time();
            $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
            // upload photo origin
            Image::make($photo)->save(public_path('/images/collection/' . $filenameOri));
        }
        // dd();exit();
        $collection = new Collection();
        $collection->title = $request->input('title');
        $collection->topic = $request->input('topic');
        $collection->photo = $filenameOri;
        $collection->save();
        return redirect('/admin/managecollections');
    }

    public function editCollection($id)
    {
        $collection = Collection::find($id);
        return view('admin.managecollections.editCollection', ['collection' => $collection]);
    }

    public function submitEditCollection(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = time();
            $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
            // upload photo origin
            Image::make($photo)->save(public_path('/images/collection/' . $filenameOri));
        }
        // dd();exit();
        $collection = Collection::find($request->input('id'));
        $collection->title = $request->input('title');
        $collection->topic = $request->input('topic');
        $collection->photo = $filenameOri;
        $collection->save();
        return redirect('/admin/managecollections');
    }

    public function deleteCollection($id)
    {
        // dd('dfdfdf');exit();
        $collection = Collection::find($id);
        $collection->delete();
        return redirect('/admin/managecollections');
    }

}