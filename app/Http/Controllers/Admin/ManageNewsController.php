<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\News;
use Image;
use Illuminate\Http\Request;
use App\Http\Requests;

class ManageNewsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
    	$News = News::all();
		return view('admin.managenews.index', ['News' => $News]);
	}

	////////////////////////////-----manage News-----////////////////////////////
    public function createNews()
    {
        return view('admin.managenews.createNews');
    }

    public function submitCreateNews(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = time();
            $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
            // upload photo origin
            Image::make($photo)->save(public_path('/images/news/' . $filenameOri));
        }
        $News = new News();
        $News->title = $request->input('title');
        $News->content = $request->input('content');
        $News->photo = $filenameOri;
        $News->save();
        return redirect('/admin/managenews');
    }

    public function editNews($id)
    {
        $News = News::find($id);
        return view('admin.managenews.editNews', ['News' => $News]);
    }

    public function submitEditNews(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $photo = $request->file('photo');
            $filename = time();
            $filenameOri = $filename . '.' . $photo->getClientOriginalExtension();
            // upload photo origin
            Image::make($photo)->save(public_path('/images/news/' . $filenameOri));
        }
        $News = News::find($request->input('id'));
        $News->title = $request->input('title');
        $News->content = $request->input('content');
        $News->photo = $filenameOri;
        $News->save();
        return redirect('/admin/managenews');
    }

    public function deleteNews($id)
    {
        // dd('dfdfdf');exit();
        $News = News::find($id);
        $News->delete();
        return redirect('/admin/managenews');
    }

}