<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use App\Categories;
use App\Charge;

class AddMoneyController extends Controller
{
	public function __construct()
	{
		$categories = Categories::where('level', 1)->get();
		// $categories = Categories::all();
		view()->share('categories', $categories);
	}

	public function payWithStripe()
	{
		return view('pages.notifyStripe');
	}
	
	public function postPaymentWithStripe(Request $request)
	{
		// validation
		// $v = Validator::make($request->all(),
		// 	[
		// 		'card_no' => 'required',
		// 		'ccExpiryMonth' => 'required',
		// 		'ccExpiryYear' => 'required',
		// 		'cvvNumber' => 'required',
		// 	],
		// 	[
		// 		'card_no.required' => 'Vui Lòng nhập card',
		// 		'ccExpiryMonth.required' => 'Vui Lòng nhập tháng',
		// 		'ccExpiryYear.required' => 'Vui Lòng nhập năm',
		// 		'cvvNumber.required' => 'Vui Lòng nhập mã cvc',
		// 	]
		// );
		// if ($v->fails()) {
		// 	return redirect()->back()->withErrors($v->Errors());
		// }
		$input = $request->all();
		$input = array_except($input,array('_token'));
		$stripe = Stripe::make(config('services.stripe.secret'));
		try {
			$token = $stripe->tokens()->create([
				'card' => [
					'number' => $request->get('card_no'),
					'exp_month' => $request->get('ccExpiryMonth'),
					'exp_year' => $request->get('ccExpiryYear'),
					'cvc' => $request->get('cvvNumber'),
				],
			]);
			if (!isset($token['id'])) {
				return redirect()->route('addmoney.paywithstripe');
			}
			$charge = $stripe->charges()->create([
				'card' => $token['id'],
				'currency' => 'USD',
				'amount' => 10,
				'description' => 'Buy product',
			]);
 
			if($charge['status'] == 'succeeded') {
				/**
				* Write Here Your Database insert logic.
				*/
				$tbl_charge = new Charge();
				$tbl_charge->stripe_id = $charge['id'];
				$tbl_charge->email = $request->get('email');
				$tbl_charge->phone = $request->get('phone_number');
				$tbl_charge->product_id = $request->get('product_id');
				$tbl_charge->quanity = $request->get('quanity');
				$tbl_charge->description = $charge['description'];
				$tbl_charge->price = (int)$charge['amount']/100;
				$tbl_charge->status = $charge['status'];
				$tbl_charge->delivery = 'pending';
				$tbl_charge->save();
				\Session::put('error','<span style="color: green;">Success</span>');
				return redirect()->route('addmoney.paywithstripe');
			} else {
				\Session::put('error','<span style="color: red;">Money not add in wallet!!</span>');
				return redirect()->route('addmoney.paywithstripe');
			}
		} catch (Exception $e) {
			\Session::put('error','<span style="color: red;">'.$e->getMessage().'</span>');
			return redirect()->route('addmoney.paywithstripe');
		} catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
			\Session::put('error','<span style="color: red;">'.$e->getMessage().'</span>');
			return redirect()->route('addmoney.paywithstripe');
		} catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
			\Session::put('error','<span style="color: red;">'.$e->getMessage().'</span>');
			return redirect()->route('addmoney.paywithstripe');
		}
	}
}
