@extends('admin.layouts.master')
@section('content')
    @if($charges->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>Stripe ID</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>description</th>
                        <th>status</th>
                        <th>delivery</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($charges as $charge)
                        <tr>
                            <td>{{ $charge->stripe_id }}</td>
                            <td>{{ $charge->email }}</td>
                            <td>{{ $charge->phone }}</td>
                            <td>{{ $charge->description }}</td>
                            <td>{{ $charge->status }}</td>
                            <td>{{ $charge->delivery }}</td>
                            <td>
                                <a href="{{ url('admin/charge/makeDone') }}/{{ $charge->id }}" class="btn btn-xs btn-info">make done</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif
@endsection