@extends('admin.layouts.master')

@section('content')
<div class="jumbotron banner-static-pages">
	<h1 class="title-static-page">Edit category</h1>
	<p class="desc-static-page">GoFundMe is the World's #1 Personal Fundraising Website.</p>
</div>
<div class="container">
	<!-- <div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div> -->
	<div class="row">
		{{ Form::open(['url' => 'submit-edit-video']) }}
		{!! Form::token() !!}
			<div class="col-md-12">
				{{ Form::label('title', 'Title:') }}
				{!! Form::text('title', $video->title, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('link', 'Link:') }}
				{{ Form::text('link', $video->link, ['class' => 'form-control']) }}
			</div>
			<div class="col-md-12">
				{{ Form::submit('SUBMIT', ['style' => 'background: blue; margin-top: 15px;', 'class'=>'form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
