@extends('admin.layouts.master')

@section('content')

	<!-- button add video -->
    <p><a href="{{ url('admin/video/create') }}" class="btn btn-success">Create Video</a></p>

    @if($video->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>title</th>
                        <th>link</th>
                        <th>Function</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($video as $v)
                        <tr>
                            <td>{{ $v->title }}</td>
                            <td>{{ $v->link }}</td>
                            <td>
                                <a href="{{ url('admin/video/edit') }}/{{ $v->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/video/delete') }}/{{ $v->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        No video
    @endif

@endsection