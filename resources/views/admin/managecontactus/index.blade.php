@extends('admin.layouts.master')

@section('content')

    <!-- button add categories -->
    <!-- <p><a href="{{ url('admin/category/create') }}" class="btn btn-success">{!! trans('quickadmin::admin.add-category') !!}</a></p> -->

    @if($messages->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone number</th>
                        <th>Message</th>
                        <th>Function</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($messages as $message)
                        <tr>
                            <td>{{ $message->full_name }}</td>
                            <td>{{ $message->email }}</td>
                            <td>{{ $message->phone_number }}</td>
                            <td>{{ $message->message }}</td>
                            <td>
                                <a href="{{ url('admin/message/delete') }}/{{ $message->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif

@endsection