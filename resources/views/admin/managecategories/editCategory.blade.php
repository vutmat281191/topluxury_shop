@extends('admin.layouts.master')

@section('content')
<div class="jumbotron banner-static-pages">
	<h1 class="title-static-page">Edit category</h1>
	<p class="desc-static-page">GoFundMe is the World's #1 Personal Fundraising Website.</p>
</div>
<div class="container">
	<div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
	<div class="row">
		{{ Form::open(['url' => 'submit-edit-category']) }}
		{!! Form::token() !!}
			<input type="hidden" name="id_category" value="{{ $category->id }}">
			<input type="hidden" name="parent_id" value="{{ $category->parent_id }}">
			<div class="col-md-12">
				{{ Form::label('name', 'Name:') }}
				{!! Form::text('name', $category->name, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				<?php
					foreach($categories as $cat){
					    $cates[$cat->id] = $cat->name; 	
					}
				?>
				{{ Form::label('parent_id', 'Category\'s Parent:') }}
				{!! Form::select('parent_id', $cates, $category->parent_id, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('level', 'Level:') }}
				{{ Form::select('level', [1=>1, 2, 3], $category->level, ['placeholder' => 'Pick a level', 'class' => 'form-control']) }}
			</div>
			<div class="col-md-12">
				{{ Form::label('content', 'Content:') }}
				{!! Form::textarea('content', $category->content, ['id'=>'tinyMce', 'class'=>'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::submit('SUBMIT', ['class'=>'form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
