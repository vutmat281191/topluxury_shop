@extends('admin.layouts.master')

@section('content')
<div class="jumbotron banner-static-pages">
	<h1 class="title-static-page">Create category</h1>
	<p class="desc-static-page">GoFundMe is the World's #1 Personal Fundraising Website.</p>
</div>
<div class="container">
	<div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
	<div class="row">
		{{ Form::open(['url' => 'submit-create-category-admin']) }}
		{!! Form::token() !!}
			<div class="col-md-12">
				{{ Form::label('name', 'Name:') }}
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				<?php
					foreach($categories as $cat){
					    $cates[$cat->id] = $cat->name; 	
					}
				?>
				{{ Form::label('parent_id', 'Category\'s Parent:') }}
				{!! Form::select('parent_id', $cates, null, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('level', 'Level:') }}
				{{ Form::select('level', [1=>1, 2, 3], null, ['placeholder' => 'Pick a level', 'class' => 'form-control']) }}
			</div>
			<div class="col-md-12">
				{{ Form::label('content', 'Content:') }}
				{!! Form::textarea('content', null, ['id'=>'tinyMce', 'class'=>'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::submit('SUBMIT', ['style' => 'background: blue; margin-top: 15px;', 'class'=>'form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
