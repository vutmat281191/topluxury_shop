@extends('admin.layouts.master')

@section('content')

	<!-- button add News -->
    <p><a href="{{ url('admin/new/create') }}" class="btn btn-success">Create News</a></p>

    @if($News->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>title</th>
                        <th>Function</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($News as $n)
                        <tr>
                            <td>{{ $n->title }}</td>
                            <td>
                                <a href="{{ url('admin/new/edit') }}/{{ $n->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/new/delete') }}/{{ $n->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        No New
    @endif

@endsection