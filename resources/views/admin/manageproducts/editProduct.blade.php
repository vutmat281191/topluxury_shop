@extends('admin.layouts.master')

@section('content')
<div class="jumbotron banner-static-pages">
	<h1 class="title-static-page">Create product</h1>
	<p class="desc-static-page">rrwerwerv sdfs df</p>
</div>
<div class="container">
	<div class="row">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	</div>
	<div class="row">
		{{ Form::open(['url' => 'submit-edit-product-admin','files' => true]) }}
		{!! Form::token() !!}
			<div class="col-md-12">
				{{ Form::label('name', 'Name:') }}
				{!! Form::text('name', $Product->name, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				<?php
					foreach($categories as $cat){
					    $cates[$cat->id] = $cat->name; 	
					}
				?>
				{{ Form::label('category', 'category:') }}
				{!! Form::select('category', $cates, $Product->category, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('size', 'Size:') }}
				<br>
				<?php
					foreach($size as $s){
					    $arr_size[$s->id] = $s->name; 	
						echo Form::checkbox('size[]', $s->id, false) . ' ' . $s->name . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
				?>
			</div>
			<div class="col-md-12">
				<?php
					foreach($color as $c){
					    $arr_color[$c->id] = $c->name; 	
					}
				?>
				{{ Form::label('color', 'Color:') }}
				{!! Form::select('color', $arr_color, $Product->color, ['placeholder' => 'Pick a category', 'class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('price', 'Price:') }}
				{!! Form::text('price', $Product->price, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('describe', 'Describe:') }}
				{!! Form::textarea('describe', $Product->describe, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('material', 'Material:') }}
				{!! Form::text('material', $Product->material, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::label('image', 'Image:') }}
				{!! Form::file('image[]', ['multiple' => 'multiple','class' => 'form-control']) !!}
			</div>
			<div class="col-md-12">
				{{ Form::submit('SUBMIT', ['class'=>'button-default form-control']) }}
			</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
