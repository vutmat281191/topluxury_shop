@extends('admin.layouts.master')

@section('content')

	<!-- button add categories -->
    <p><a href="{{ url('admin/product/create') }}" class="btn btn-success">Add Product</a></p>

    @if($Products->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Link</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($Products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->link }}</td>
                            <td>
                                <a href="{{ url('admin/product/edit') }}/{{ $product->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/product/delete') }}/{{ $product->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif

@endsection