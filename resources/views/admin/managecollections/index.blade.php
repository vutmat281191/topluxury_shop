@extends('admin.layouts.master')

@section('content')

	<!-- button add collection -->
    <p><a href="{{ url('admin/collection/create') }}" class="btn btn-success">Create Collection</a></p>

    @if($collection->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.users-index-users_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>title</th>
                        <th>topic</th>
                        <th>photo</th>
                        <th>Function</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($collection as $co)
                        <tr>
                            <td>{{ $co->title }}</td>
                            <td>{{ $co->topic }}</td>
                            <td>
                                <img style="height: 50px; width: 50px;" src="{{ url('public/images/collection') }}/{{ $co->photo }}">
                            </td>
                            <td>
                                <a href="{{ url('admin/collection/edit') }}/{{ $co->id }}" class="btn btn-xs btn-info">{!! trans('quickadmin::admin.users-index-edit') !!}</a>
                                <a href="{{ url('admin/collection/delete') }}/{{ $co->id }}" class="btn btn-xs btn-danger">{!! trans('quickadmin::admin.users-index-delete') !!}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        No collection
    @endif

@endsection