<div  id="fixtopbar" class="main-jumbo top-banner-menu" >	
	<div class="container-fluid navbar-fixed-top">		
		<div class="navbar-header">
			<!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> -->
			<button type="button" class="navbar-toggle" onclick="openNav()">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="nav_fix_top" class="navbar-collapse main-menu-fixtop" >
			<nav id="nav">
				<ul>
					<li><a class="bf-menubar" style="float: left; display: none;" href="{{ url('/') }}"><img  src="{{url('public/images/logo-biluxury.jpg') }}" alt="logohere"></a></li> 
					<!-- Menu tooggle on smaller screens  -->
					<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					<li><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li><a style="font-weight: 600;" href="{{ url('/hot-sales') }}">Hot Sales</a></li>
					<!-- Simple menu item without sub menu -->
					@foreach ($categories as $cat)
						<li>
							<?php $childLevel2 = App\Categories::getChildrenCategory($cat->id, 2); ?>
							@if ($childLevel2->count()>0)
								<a href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}<b class="caret"></b></a>
								<ul>
									@foreach ($childLevel2 as $child2)
									<li>
										<?php $childLevel3 = App\Categories::getChildrenCategory($child2->id, 3); ?>
										@if ($childLevel3->count()>0)
											<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}<b class="caret"></b></a>
											<ul>
												@foreach ($childLevel3 as $child3)
												<li><a href="{{ url('category') }}/{{ $child3->link }}">{{ $child3->name }}</a></li>
												@endforeach
											</ul>
										@else
											<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}</a>
										@endif	
									</li>
									@endforeach
									<li><img src="https://vn-live.slatic.net/v2/resize/products/S-17465-fc254e430e087f437e896a0701fd5969.jpg"></li>
								</ul>
							@else
								<a href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}</a>
							@endif
						</li>
					@endforeach
					<li><a href="{{ url('/') }}">Lady luxury</a></li>
				</ul>
			</nav>
			<div class="nav navbar-nav navbar-right search-right-clm">
				{{ Form::open(['url' => 'submit-search-product','class' =>'form-search-template-wrapper']) }}
			      	{!! Form::token() !!}
			        {!! Form::text('search', isset($text_search)?$text_search:null, ['placeholder' => 'Tìm kiếm', 'class' => 'search-campaign-bar']) !!}
					<button type="submit"><i class="fa fa-search"></i></button>
			    {!! Form::close() !!}
				<div class="shopping-cart">
					<a href="{{ url('/cart') }}">
						<i class="cart-in-menu fa fa-shopping-cart" aria-hidden="true">
							<span class="num"><?php echo isset($_COOKIE["cart-num"]) ? $_COOKIE["cart-num"] : 0 ?></span>
						</i>
					</a>
				</div>				
			</div>
		</div>
		<div class="search-right-clm-mobile">
				{{ Form::open(['url' => 'submit-search-product','class' =>'form-search-template-wrapper']) }}
			      	{!! Form::token() !!}
			        {!! Form::text('search', null, ['placeholder' => 'Tìm kiếm', 'class' => 'search-campaign-bar']) !!}
					<button type="submit"><i class="fa fa-search"></i></button>
			    {!! Form::close() !!}
			</div>
	</div>
</div>
<script type="text/javascript">
jQuery("document").ready(function($) {

    var nav = $('.navbar-fixed-top');

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });
</script>