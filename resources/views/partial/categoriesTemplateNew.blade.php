<div class="col-md-12">
	<div class="col-md-12">
		<h2 class="title-article" style="font-weight: bold;">NEWS</h2>
	</div>
</div>
<div class="col-md-12">
	@foreach ($News as $n)
	    <div class="col-md-4" style="margin-bottom: 20px;">
	    	<div style="height: 260px; overflow: hidden;"  class="image-wrapper">
	    		<img style="height: 260px;" src="{{ url('/public/images/news') }}/{{ $n->photo }}">
	    	</div>
	    	<h2>{{ $n->title }}</h2>
	    	<div class="content-new" style="height: 0px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">{!! $n->content !!}</div>
	    </div>
	@endforeach
</div>