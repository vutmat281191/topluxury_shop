<div class="col-md-12">
	<div class="col-md-12">
		<h2 class="title-article" style="font-weight: bold;">VIDEO</h2>
	</div>
</div>
<div class="col-md-12">
	<div class="col-md-6" style="text-align: center;"><h2>Xuân Hè</h2></div>
	<div class="col-md-6" style="text-align: center;"><h2>Thu Đông</h2></div>
</div>
<div class="col-md-12">
	@foreach ($collection as $c)
	    @if ($c->topic == 'Xuân Hè')
		    <div style="height: 460px; overflow: hidden; margin-bottom: 20px; width: 49.5%; float: left;" class="summer-part">
		    	<img width="100%" src="{{ url('/public/images/collection') }}/{{ $c->photo }}">
		    </div>
		@else
		    <div style="height: 460px; overflow: hidden; margin-bottom: 20px; width: 49.5%; float: right;" class="winter-part">
		    	<img width="100%" src="{{ url('/public/images/collection') }}/{{ $c->photo }}">
		    </div>
		@endif
	@endforeach
</div>