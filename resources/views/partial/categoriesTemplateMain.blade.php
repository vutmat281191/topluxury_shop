<div class="col-md-3" style="margin-top: 18px;">
	<nav class="nav_menu left-category">
		<ul>
			<!-- Simple menu item without sub menu -->
			@foreach ($categories as $cat)
			<li>
				<?php $childLevel2 = App\Categories::getChildrenCategory($cat->id, 2); ?>
				@if ($childLevel2->count()>0)
				<a class="caret-parent" href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}
					<b class="caret click-menu-show close-m"></b>
				</a>
				<ul>
					@foreach ($childLevel2 as $child2)
					<li class="item-menu-show">
						<?php $childLevel3 = App\Categories::getChildrenCategory($child2->id, 3); ?>
						@if ($childLevel3->count()>0)
						<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}<b class="caret"></b></a>
						<ul>
							@foreach ($childLevel3 as $child3)
							<li><a href="{{ url('category') }}/{{ $child3->link }}">{{ $child3->name }}</a></li>
							@endforeach
						</ul>
						@else
						<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}</a>
						@endif
					</li>
					@endforeach
				</ul>
				@else
				<a href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}</a>
				@endif
			</li>
			@endforeach
		</ul>
	</nav>
	<div class="filter-color">
		<h3>Color</h3>
		<ul class="nav navbar-nav left-menu">
			<?php $color = App\Products::get(['color'])->toArray();
			$color2 = array_map(function($a) {return array_pop($a); }, $color);
			$color3 = array_unique($color2);
			?>
			@foreach ($color3 as $c)
			<li>
				<?php
				$color_option = App\Color::find($c)->variable;
				if (strpos(url()->full(), '?'))
					$url_color = url()->full() . '&color=' . $color_option;
				else $url_color = url()->full() . '?color=' . $color_option;

				
				?>
				<a href="{{ $url_color }}">
					<span class="color-product" style="display: block; width: 15px; height: 15px; border-radius: 10px; border: 1px solid; background: {{ App\Color::find($c)->code }};"></span>
				</a>
			</li>
			@endforeach
		</ul>
	</div>
	<div class="filter-size">
		<h3>Size</h3>
		<ul class="nav navbar-nav left-menu">
			<?php $size = App\Products::get(['size'])->toArray();
			$size2 = array_map(function($a) {  return array_pop($a); }, $size);
			$size3 = implode(",",$size2);
			$size4 = explode(",",$size3);
			$size5 = array_unique($size4);
			?>
			@foreach ($size5 as $s)
			<?php
			$size_option = App\Size::find($s)->name;
			if (strpos(url()->full(), '?'))
			$url_size = url()->full() . '&size=' . $size_option;
			else $url_size = url()->full() . '?size=' . $size_option;
			?>
			<li><a href="{{ $url_size }}">{{ $size_option }}</a></li>
			@endforeach
		</ul>
	</div>
</div>
<div class="col-md-9">
	<div class="col-md-12 filter-part" style="display: flex; justify-content: flex-end;">
		<div class="dropdown">
		  	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter</button>
		  	<ul class="dropdown-menu">
		  		<?php
		  			if (strpos(url()->full(), '?')) {
						$url_price_asc = url()->full() . '&price=asc';
						$url_price_desc = url()->full() . '&price=desc';
						$url_create_desc = url()->full() . '&created_at=desc';
						$url_filter_grid = url()->full() . '&grid=';
		  			}
					else {
						$url_price_asc = url()->full() . '?price=asc';
						$url_price_desc = url()->full() . '?price=desc';
						$url_create_desc = url()->full() . '?created_at=desc';
						$url_filter_grid = url()->full() . '?grid=';
					}
		  		?>
		    	<li><a href="{{ $url_create_desc }}">Newest</a></li>
		    	<li><a href="{{ $url_price_asc }}">Price increase</a></li>
		    	<li><a href="{{ $url_price_desc }}">Price discrease</a></li>
		  	</ul>
		</div>
		<div class="filter-grid">
		    <a href="{{ $url_filter_grid }}3">
		        <img src="{{ url('/public/images/grid-3.png') }}" alt="">
		    </a>
		    <a href="{{ $url_filter_grid }}5">
		        <img src="{{ url('/public/images/grid-5.png') }}" alt="">
		    </a>
		</div>
	</div>
	@foreach ($list_products as $item)
	<div class="{{ $class_grid }} item_product_list">
		<div class="border">
			<span class="icon_new"></span><span><span class="discount"></span></span>
			<div class="img">
				<a href="{{ url('/product') }}/{{ $item->link }}" title="{{ $item->name }}">
					<?php $array_item = explode(",", rtrim($item->image, ",")); ?>
					<img src="{{ url('/public/images/products') }}/{{ $array_item[0] }}" width="387" title="{{ $item->name }}" alt="{{ $item->name }}" class="first" style="opacity: 1;">
					<img src="{{ url('/public/images/products') }}/{{ $array_item[1] }}" width="387" title="{{ $item->name }}" alt="{{ $item->name }}" class="second" style="opacity: 0;">
				</a>
				<span class="loader">&nbsp;</span>
			</div>
			<div class="info">
				<h3 class="clb"><a href="{{ url('/product') }}/{{ $item->link }}" title="{{ $item->name }}">{{ $item->name }}</a></h3>
				<div class="brief clearfix">
					<p><span class="price-old"></span> {{ $item->price }} đ</p>
				</div>
				<div><span class="color-dot" style=" background: {{ App\Color::find($item->color)->code }}"></span></div>
			</div>
		</div>
	</div>
	@endforeach
</div>