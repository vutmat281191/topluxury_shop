<div class="col-md-3">
	<nav class="nav_menu left-category">
		<ul>
			<!-- Simple menu item without sub menu -->
			@foreach ($categories as $cat)
			<li>
				<?php $childLevel2 = App\Categories::getChildrenCategory($cat->id, 2); ?>
				@if ($childLevel2->count()>0)
				<a href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}<b class="caret"></b></a>
				<ul>
					@foreach ($childLevel2 as $child2)
					<li>
						<?php $childLevel3 = App\Categories::getChildrenCategory($child2->id, 3); ?>
						@if ($childLevel3->count()>0)
						<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}<b class="caret"></b></a>
						<ul>
							@foreach ($childLevel3 as $child3)
							<li><a href="{{ url('category') }}/{{ $child3->link }}">{{ $child3->name }}</a></li>
							@endforeach
						</ul>
						@else
						<a href="{{ url('category') }}/{{ $child2->link }}">{{ $child2->name }}</a>
						@endif
					</li>
					@endforeach
				</ul>
				@else
				<a href="{{ url('category') }}/{{ $cat->link }}">{{ $cat->name }}</a>
				@endif
			</li>
			@endforeach
		</ul>
	</nav>
</div>
<div class="col-md-9">
	{!! $cat_content !!}
</div>