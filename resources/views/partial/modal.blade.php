<!-- modal -->
<div class="modal fade" id="tableSizeTutorial" tabindex="-1" role="dialog" aria-labelledby="tableSizeTutorialLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="tableSizeTutorialLabel">Guide to choose size</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p><strong>BẢNG MÃ SIZE ÁO SƠ MI CỘC TAY</strong></p>
				<table style="width: 454px;">
					<tbody>
						<tr style="height: 35px;">
							<td style="width: 129px; height: 70px;" rowspan="2">
								<p>Size</p>
							</td>
							<td style="width: 80px; height: 35px;">
								<p>M</p>
							</td>
							<td style="width: 78px; height: 35px;">
								<p>L</p>
							</td>
							<td style="width: 74px; height: 35px;">
								<p>XL</p>
							</td>
							<td style="width: 77px; height: 35px;">
								<p>XXL</p>
							</td>
						</tr>
						<tr style="height: 35px;">
							<td style="width: 80px; height: 35px;">
								<p>cm</p>
							</td>
							<td style="width: 78px; height: 35px;">
								<p>cm</p>
							</td>
							<td style="width: 74px; height: 35px;">
								<p>cm</p>
							</td>
							<td style="width: 77px; height: 35px;">
								<p>cm</p>
							</td>
						</tr>
						<tr style="height: 48px;">
							<td style="width: 129px; height: 48px;">
								<p>Ngực (đo dưới nách)</p>
							</td>
							<td style="width: 80px; height: 48px;">
								<p>100</p>
							</td>
							<td style="width: 78px; height: 48px;">
								<p>104</p>
							</td>
							<td style="width: 74px; height: 48px;">
								<p>108</p>
							</td>
							<td style="width: 77px; height: 48px;">
								<p>112</p>
							</td>
						</tr>
						<tr style="height: 35px;">
							<td style="width: 129px; height: 35px;">
								<p>Vai</p>
							</td>
							<td style="width: 80px; height: 35px;">
								<p>45.1</p>
							</td>
							<td style="width: 78px; height: 35px;">
								<p>46.6</p>
							</td>
							<td style="width: 74px; height: 35px;">
								<p>48.1</p>
							</td>
							<td style="width: 77px; height: 35px;">
								<p>49.6</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p>&nbsp;</p>
				<p><strong>BẢNG MÃ SIZE ÁO SƠ MI DÀI TAY</strong></p>
				<table style="width: 442px;">
					<tbody>
						<tr>
							<td style="width: 125px;" rowspan="2">
								<p>Size</p>
							</td>
							<td style="width: 77px;">
								<p>M</p>
							</td>
							<td style="width: 72px;">
								<p>L</p>
							</td>
							<td style="width: 71px;">
								<p>XL</p>
							</td>
							<td style="width: 73px;">
								<p>XXL</p>
							</td>
						</tr>
						<tr>
							<td style="width: 77px;">
								<p>cm</p>
							</td>
							<td style="width: 72px;">
								<p>cm</p>
							</td>
							<td style="width: 71px;">
								<p>cm</p>
							</td>
							<td style="width: 73px;">
								<p>cm</p>
							</td>
						</tr>
						<tr>
							<td style="width: 125px;">
								<p>Ngực (đo dưới nách)</p>
							</td>
							<td style="width: 77px;">
								<p>99</p>
							</td>
							<td style="width: 72px;">
								<p>103</p>
							</td>
							<td style="width: 71px;">
								<p>107</p>
							</td>
							<td style="width: 73px;">
								<p>111</p>
							</td>
						</tr>
						<tr>
							<td style="width: 125px;">
								<p>Vai</p>
							</td>
							<td style="width: 77px;">
								<p>45.1</p>
							</td>
							<td style="width: 72px;">
								<p>46.6</p>
							</td>
							<td style="width: 71px;">
								<p>48.1</p>
							</td>
							<td style="width: 73px;">
								<p>49.6</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p>&nbsp;</p>
				<p><strong>BẢNG MÃ SIZE ÁO (chung)</strong></p>
				<table style="width: 437px;">
					<tbody>
						<tr style="height: 35px;">
							<td style="height: 70px; width: 102px;" rowspan="2">
								<p>Size</p>
							</td>
							<td style="height: 35px; width: 68px;">
								<p>M</p>
							</td>
							<td style="height: 35px; width: 54px;">
								<p>L</p>
							</td>
							<td style="height: 35px; width: 62px;">
								<p>XL</p>
							</td>
							<td style="height: 35px; width: 56px;">
								<p>XXL</p>
							</td>
						</tr>
						<tr style="height: 35px;">
							<td style="height: 35px; width: 68px;">
								<p>cm</p>
							</td>
							<td style="height: 35px; width: 54px;">
								<p>cm</p>
							</td>
							<td style="height: 35px; width: 62px;">
								<p>cm</p>
							</td>
							<td style="height: 35px; width: 56px;">
								<p>cm</p>
							</td>
						</tr>
						<tr style="height: 48px;">
							<td style="height: 48px; width: 102px;">
								<p>Ngực (đo dưới nách)</p>
							</td>
							<td style="height: 48px; width: 68px;">
								<p>100</p>
							</td>
							<td style="height: 48px; width: 54px;">
								<p>104</p>
							</td>
							<td style="height: 48px; width: 62px;">
								<p>108</p>
							</td>
							<td style="height: 48px; width: 56px;">
								<p>112</p>
							</td>
						</tr>
						<tr style="height: 35px;">
							<td style="height: 35px; width: 102px;">
								<p>Vai</p>
							</td>
							<td style="height: 35px; width: 68px;">
								<p>45.1</p>
							</td>
							<td style="height: 35px; width: 54px;">
								<p>46.6</p>
							</td>
							<td style="height: 35px; width: 62px;">
								<p>48.1</p>
							</td>
							<td style="height: 35px; width: 56px;">
								<p>49.6</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- modal guide to buy -->
<div class="modal fade" id="tableGuideToBuy" tabindex="-1" role="dialog" aria-labelledby="tableGuideToBuyLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="tableGuideToBuyLabel">Guide to buy</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div>BƯỚC 1: &nbsp;CHỌN MUA SẢN PHẨM.</div>
				<div>Tại website www.biluxury.vn, chọn sản phẩm quý khách quan tâm (màu sắc, kích cỡ)</div>
				<div>&nbsp;</div>
				<div>BƯỚC 2: KIỂM TRA THÔNG TIN SẢN PHẨM TRONG GIỎ HÀNG.</div>
				<div>&nbsp;</div>
				<div>BƯỚC 3: NHẬP THÔNG TIN NGƯỜI MUA HÀNG.&nbsp;</div>
				<div>Để thuận tiện hơn trong việc giao hàng, quý khách hàng hãy điền đầy đủ thông tin cần thiết để Biluxury phục vụ khách hàng tốt nhất và nhanh nhất.</div>
				<div>Nhân viên Chăm sóc khách hàng của Biluxury sẽ gọi điện lại để xác nhận tình trạng còn hàng/ hết hàng của sản phẩm trong đơn hàng qua số điện thoại Quý khách đã cung cấp trong vòng 24h của các ngày làm việc.</div>
				<div>&nbsp;</div>
				<div>BƯỚC 4: LỰA CHỌN HÌNH THỨC THANH TOÁN &amp; VẬN CHUYỂN.</div>
				<div>&nbsp;</div>
				<div>BƯỚC 5: NHẬN HÀNG.</div>
				<div>Sau 12h – 48h kể từ khi hoàn tất thanh toán và TK của công ty báo tiền đã được chuyển, quý khách sẽ nhận được hàng từ dịch vụ chuyển phát nhanh. Nếu quý khách mặc không vừa size, vui lòng liên lạc lại ngay với nhân viên chăm sóc để đăng k&lrm;ý đổi size. Trường hợp này, quý khách vui lòng thanh toán phí chuyển phát hàng hóa phát sinh.</div>
			</div>
		</div>
	</div>
</div>

<!-- modal input card -->
<div class="modal fade" id="modalFormCard" tabindex="-1" role="dialog" aria-labelledby="modalFormCardLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalFormCardLabel">Form Payment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				{{ Form::open(['url' => route('addmoney.stripe'), 'class' => 'form-horizontal']) }}
					{!! Form::token() !!}
					<div class="col-xs-12 form-group card required">
						{{ Form::label('email', 'Email:') }}
						{!! Form::email('email', null, ['class' => 'form-control', 'required' => 'true']) !!}
					</div>
					<div class="col-xs-12 form-group card required">
						{{ Form::label('phone_number', 'Phone number:') }}
						{!! Form::number('phone_number', null, ['class' => 'form-control', 'size' => '15', 'required' => 'true']) !!}
					</div>
					<div class="col-xs-12 form-group card required">
						{{ Form::label('card_no', 'Card ID:') }}
						{!! Form::number('card_no', null, ['class' => 'form-control', 'autocomplete' => 'off', 'size' => '20', 'required' => 'true']) !!}
					</div>
					<div class="col-xs-12 form-group expiration required">
						{{ Form::label('ccExpiryMonth', 'Exp month:') }}
						{!! Form::number('ccExpiryMonth', null, ['class' => 'form-control card-expiry-month', 'placeholder' => 'MM', 'min' => '1', 'max' => '12', 'required' => 'true']) !!}
					</div>
					<div class="col-xs-12 form-group expiration required">
						{{ Form::label('ccExpiryYear', 'Exp year:') }}
						{!! Form::number('ccExpiryYear', null, ['class' => 'form-control', 'placeholder' => 'YYYY', 'min' => '2018', 'max' => '2050', 'required' => 'true']) !!}
					</div>
					<div class="col-xs-12 form-group cvc required">
						{{ Form::label('cvvNumber', 'Cvc:') }}
						{!! Form::password('cvvNumber', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'ex: 311', 'size' => '4', 'required' => 'true']) !!}
					</div>
					<input id="quanity_of_product" type="hidden" name="quanity" value="1">
					<input type="hidden" name="product_id" value="{{ $product->id }}">
					<button type="submit" class="button-buy">Submit</span></button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>