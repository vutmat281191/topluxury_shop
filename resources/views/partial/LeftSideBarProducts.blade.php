<div class="col-lg-2 col-lg-pull-10" style="padding:0">
	<div id="s-left" class="s-mod">
		<div class="s-block">
			<div class="s-block-content">
				<ul class="ver-down-cat">
					<li class="category " style="opacity: 1;">
						<a href="http://biluxury.vn/qua-tang-noel" data-id="126">
							<span><span>QUÀ TẶNG NOEL</span></span>
						</a>
					</li>
					<li class="category has_dropdown" style="opacity: 1;">
						<a href="http://biluxury.vn/ao" data-id="11">
							<span><span>Áo</span></span>
						</a>
						<div class="open">
							<span></span>
						</div>
						<ul class="dropdown" style="display: block;">
							<li class="has-dropdown">
								<a href="http://biluxury.vn/ao/ao-somi">
									Áo sơmi
								</a>
								<ul class="dropdown" style="display: block;">
									<li class="">
										<a href="http://biluxury.vn/ao/so-mi-bamboo-sieu-mat">
											Sơ mi Bamboo - siêu mát
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/so-mi-spun-chong-nhan">
											Sơ mi Spun chống nhăn
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-so-mi-coc-tay">
											Áo sơ mi cộc tay
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-so-mi-dai-tay">
											Áo sơ mi dài tay
										</a>
									</li>
								</ul>
							</li>
							<li class="has-dropdown">
								<a href="http://biluxury.vn/ao/ao-thun---len---ni">
									Áo thun - Len - Nỉ
								</a>
								<ul class="dropdown" style="display: block;">
									<li class="">
										<a href="http://biluxury.vn/ao/ao-giu-nhiet">
											Áo giữ nhiệt
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-thun-dai-tay">
											Áo thun dài tay
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-len-hoa-tiet-">
											Áo len họa tiết
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-len-tron-">
											Áo len trơn
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-ni-">
											Áo nỉ
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/bo-gym">
											Bộ gym
										</a>
									</li>
								</ul>
							</li>
							<li class="has-dropdown">
								<a href="http://biluxury.vn/ao/ao-khoac">
									Áo khoác
								</a>
								<ul class="dropdown" style="display: block;">
									<li class="">
										<a href="http://biluxury.vn/ao/ao-khoac-ni">
											Áo khoác nỉ
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-khoac-gio-1-lop">
											Áo khoác gió 1 lớp
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-khoac-gio-2-lop">
											Áo khoác gió 2 lớp
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-khoac-da">
											Áo khoác da
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-khoac-phao">
											Áo khoác phao
										</a>
									</li>
								</ul>
							</li>
							<li class="has-dropdown">
								<a href="http://biluxury.vn/ao/ao-vest---mangto">
									Áo Vest - Mangto
								</a>
								<ul class="dropdown" style="display: block;">
									<li class="">
										<a href="http://biluxury.vn/ao/vest-cuoi---vest-cong-so">
											Vest cưới - Vest công sở
										</a>
									</li>
									<li class="">
										<a href="http://biluxury.vn/ao/ao-mang-to">
											Áo Măng Tô
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="category has_dropdown" style="opacity: 1;">
						<a href="http://biluxury.vn/quan" data-id="12">
							<span><span>Quần</span></span>
						</a>
						<div class="open">
							<span></span>
						</div>
						<ul class="dropdown" style="display: block;">
							<li class="">
								<a href="http://biluxury.vn/quan/quan-au">
									Quần âu
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/quan/quan-jeans">
									Quần Jeans
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/quan/quan-kaki">
									Quần Kaki
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/quan/quan-ngo">
									Quần ngố
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/quan/quan-ni-">
									Quần nỉ
								</a>
							</li>
						</ul>
					</li>
					<li class="category " style="opacity: 1;">
						<a href="http://biluxury.vn/set-" data-id="136">
							<span><span>Set </span></span>
						</a>
					</li>
					<li class="category selected has_dropdown" style="opacity: 1;">
						<a href="http://biluxury.vn/phu-kien" class="selected" data-id="13">
							<span><span>Phụ kiện</span></span>
						</a>
						<div class="open">
							<span></span>
						</div>
						<ul class="dropdown" style="display: block;">
							<li class="">
								<a href="http://biluxury.vn/phu-kien/cavat">
									Cavat
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/phu-kien/day-lung-nam">
									Dây lưng nam
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/phu-kien/giay-nam">
									Giày nam
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/phu-kien/kinh">
									Kính
								</a>
							</li>
							<li class="">
								<a href="http://biluxury.vn/phu-kien/vi-da">
									Ví da
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="clb"></div>
		</div>
		<div class="s-block attribute clearfix">
			<div class="s-block-title-bg"><h3><a>Màu sắc</a></h3></div>
			<div class="s-block-content">
				<ul class="attr color">
					
					<li title="Đen" style="background:#000000" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+den'" class="color">
						<a title="Đen" href="http://biluxury.vn/phu-kien/m+den" class="color"> Đen (25)</a>
					</li>
					
					<li title="Trắng" style="background:#ffffff" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+trang'" class="color">
						<a title="Trắng" href="http://biluxury.vn/phu-kien/m+trang" class="color"> Trắng (2)</a>
					</li>
					
					<li title="Xanh lá" style="background:#44cc44" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+xanh-la'" class="color">
						<a title="Xanh lá" href="http://biluxury.vn/phu-kien/m+xanh-la" class="color"> Xanh lá (1)</a>
					</li>
					
					<li title="Xanh tím than" style="background:#291c87" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+tim-than'" class="color">
						<a title="Xanh tím than" href="http://biluxury.vn/phu-kien/m+tim-than" class="color"> Xanh tím than (3)</a>
					</li>
					
					<li title="Nâu" style="background:#432424" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+nau'" class="color">
						<a title="Nâu" href="http://biluxury.vn/phu-kien/m+nau" class="color"> Nâu (11)</a>
					</li>
					
					<li title="Xanh tím than đậm" style="background:#0c0c3b" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+xanh-tim-than-dam'" class="color">
						<a title="Xanh tím than đậm" href="http://biluxury.vn/phu-kien/m+xanh-tim-than-dam" class="color"> Xanh tím than đậm (6)</a>
					</li>
					
					<li title="Hồng" style="background:#ff00ff" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+hong'" class="color">
						<a title="Hồng" href="http://biluxury.vn/phu-kien/m+hong" class="color"> Hồng (2)</a>
					</li>
					
					<li title="Tím" style="background:#9a03ff" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+tim'" class="color">
						<a title="Tím" href="http://biluxury.vn/phu-kien/m+tim" class="color"> Tím (4)</a>
					</li>
					
					<li title="Đỏ đô" style="background:#5b0404" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+do-do'" class="color">
						<a title="Đỏ đô" href="http://biluxury.vn/phu-kien/m+do-do" class="color"> Đỏ đô (2)</a>
					</li>
					
					<li title="Nâu sáng" style="background:#c9b7a1" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+nau-sang'" class="color">
						<a title="Nâu sáng" href="http://biluxury.vn/phu-kien/m+nau-sang" class="color"> Nâu sáng (2)</a>
					</li>
					
					<li title="Xanh da trời" style="background:#3c78d8" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/m+xanh-da-troi'" class="color">
						<a title="Xanh da trời" href="http://biluxury.vn/phu-kien/m+xanh-da-troi" class="color"> Xanh da trời (1)</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="s-block attribute clearfix">
			<div class="s-block-title-bg"><h3><a>Kích cỡ</a></h3></div>
			<div class="s-block-content">
				<ul class="attr ">
					
					<li title="38" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/o+38'">
						<a title="38" href="http://biluxury.vn/phu-kien/o+38"> 38 (6)</a>
					</li>
					
					<li title="40" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/o+40'">
						<a title="40" href="http://biluxury.vn/phu-kien/o+40"> 40 (52)</a>
					</li>
					
					<li title="42" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/o+42'">
						<a title="42" href="http://biluxury.vn/phu-kien/o+42"> 42 (6)</a>
					</li>
					
					<li title="39" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/o+39'">
						<a title="39" href="http://biluxury.vn/phu-kien/o+39"> 39 (15)</a>
					</li>
					
					<li title="41" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/o+41'">
						<a title="41" href="http://biluxury.vn/phu-kien/o+41"> 41 (7)</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="s-block attribute clearfix">
			<div class="s-block-title-bg"><h3><a>Giá</a></h3></div>
			<div class="s-block-content">
				<ul class="attr ">
					<li title="Dưới 300.000 VNĐ" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/p+300'">
						<a title="Dưới 300.000 VNĐ" href="http://biluxury.vn/phu-kien/p+300"> Dưới 300.000 VNĐ (8)</a>
					</li>
					
					<li title="300.000 - 500.000 VNĐ" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/p+300-500'">
						<a title="300.000 - 500.000 VNĐ" href="http://biluxury.vn/phu-kien/p+300-500"> 300.000 - 500.000 VNĐ (48)</a>
					</li>
					
					<li title="Trên 1.000.000" style="background:" onclick="javascript:window.location.href='http://biluxury.vn/phu-kien/p+tren-1000000'">
						<a title="Trên 1.000.000" href="http://biluxury.vn/phu-kien/p+tren-1000000"> Trên 1.000.000 (6)</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
</div>