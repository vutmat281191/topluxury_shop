@extends('layouts.app')

@section('content')
<div class="container-fluid home-page">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="{{ url('public/images/slider-home') }}/slide1.jpg" alt="Los Angeles" style="width:100%;">
			</div>

			<div class="item">
				<img src="{{ url('public/images/slider-home') }}/slide2.jpg" alt="Chicago" style="width:100%;">
			</div>
			
			<div class="item">
				<img src="{{ url('public/images/slider-home') }}/slide3.jpg" alt="New york" style="width:100%;">
			</div>

			<div class="item">
				<img src="{{ url('public/images/slider-home') }}/slide4.jpg" alt="New york" style="width:100%;">
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<div class="newest-produces">
		<h3>New Products</h3>
		<div class="row">
			<div class="col-md-6 item-products">
				<img src="http://biluxury.vn/media/lookbook/1513247235_da.jpg">
			</div>
			<div class="col-md-6 item-products">
				<img src="http://biluxury.vn/media/lookbook/1513247235_da.jpg">
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 item-products">
				<img src="http://biluxury.vn/media/lookbook/1513247235_da.jpg">
			</div>
			<div class="col-md-4 item-products">
				<img src="http://biluxury.vn/media/lookbook/1513247235_da.jpg">
			</div>
			<div class="col-md-4 item-products">
				<img src="http://biluxury.vn/media/lookbook/1513247235_da.jpg">
			</div>
		</div>
	</div>
	<h3>Register to get information</h3>
	<div class="row register-information">
		
	</div>
</div>

@endsection