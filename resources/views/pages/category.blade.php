@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row category-page">
		@if ($template == 'main')
		    @include('partial.categoriesTemplateMain')
		@elseif ($template == 'collection')
		    @include('partial.categoriesTemplateCollection')
		@elseif ($template == 'video')
		    @include('partial.categoriesTemplateVideo')
		@elseif ($template == 'new')
		    @include('partial.categoriesTemplateNew')
		@else
			@include('partial.categoriesTemplateStatic')
		@endif
	</div>
</div>
@endsection