@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<!-- <div class="col-md-3"></div> -->
		<div class="col-md-12">
			<div id="s-content" class="clearfix">                    
				<div class="content contact">
					<div><h2>CÔNG TY CP THỜI TRANG BIMART</h2>
				<p>Địa chỉ: 105 Nguyễn Xiển, Thanh Xuân,&nbsp;Hà Nội<br> Đại diện: Lê Xuân Tùng.<br> Số DKKD: 0105256513 cấp ngày 20/07/2011 tại Phòng ĐKKD - Sở Kế Hoạch và Đầu Tư TP.Hà Nội<br>Hotline: 094 23 888 24<br> Email: Thoitrangnambiluxury@gmail.com<br> Website: http://biluxury.vn - Fanpage: https://www.facebook.com/biluxury.vn</p>
				<h3>GIỜ MỞ CỬA</h3>
				<p>Thứ 2 đến Chủ nhật: 8h - 22h<br> Call us: 0944 32 89 89</p>
				<h3>HỖ TRỢ MUA HÀNG</h3>
				<p>Ms Hằng - 0944 32 89 89 hoặc&nbsp;094 23 888 24</p>
				<p>Fanpage: fb.com/Biluxury.vn</p>
				<h3>LIÊN HỆ LÀM ĐẠI LÝ</h3>
				<p>Mr Tuấn&nbsp;- 093 646 82 68</p>
				<p><br> HỢP TÁC CÙNG BILUXURY - VỮNG CHẮC NGAY TỪ BƯỚC KHỞI ĐẦU!</p><br></div>
					{{ Form::open(['url' => 'submit-create-contact-us']) }}
					{!! Form::token() !!}
						<div>
							{{ Form::label('full_name', 'Full Name:') }}
							{!! Form::text('full_name', null, ['class' => 'form-control']) !!}
						</div>
						<div>
							{{ Form::label('email', 'Email:') }}
							{!! Form::email('email', null, ['class' => 'form-control']) !!}
						</div>
						<div>
							{{ Form::label('phone_number', 'Phone number:') }}
							{!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
						</div>
						<div>
							{{ Form::label('message', 'Message:') }}
							{!! Form::textarea('message', null, ['class'=>'form-control']) !!}
						</div>
						<div class="btn-contact-us">
							{{ Form::submit('GỬI LỜI NHẮN', ['class'=>'button-default form-control']) }}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection