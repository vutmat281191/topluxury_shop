@extends('layouts.app')
@section('content')
<div class='container'>
	<input type="button" value="Go back!" onclick="history.back()">
	<h2>{!! Session::get('error') !!}</h2>
</div>
@endsection