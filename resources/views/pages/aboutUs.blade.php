@extends('layouts.app')
@section('content')
<div class="container-fluid home-page">
	<div class="col-md-3"></div>
	<div class="col-md-9">
		<div id="s-content" class="clearfix">
			<div class="content">
				<h1 class="title">Lịch sử thương hiệu <span></span></h1>
				<div class="arrow clearfix">
					<div class="fb-like col-lg-12 clb" data-send="true" data-width="" data-show-faces="true" style="padding:10px 0;"></div>
				</div>
				<div class="clb"><br></div>
				<div class="img-hover"><p><span style="font-size: 16px;"><strong>Biluxury</strong> tiền thân là Bionline, là thương hiệu thời trang nam của Công ty CP thời trang Bimart, ra đời từ năm 2009, tiên phong phát triển theo hình thức chuỗi nhượng quyền và nhanh chóng đạt được nhiều thành tích ấn tượng.</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;"><strong>Tháng 7 năm 2014</strong> Biluxury ra đời. Th</span><span style="font-size: 16px;">ương hiệu là tập hợp đầy đủ các yếu tố của chuỗi với một định vị mới, một diện mạo tách biệt so với Bionline. Tính đến tháng 12/2017, chỉ sau 3 năm phát triển, Biluxury đã sở hữu 75 showroom trên toàn quốc trên mục tiêu 300 showroom vào năm 2019.</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;">Biluxury được biết đến là thương hiệu thời trang nam tiên phong trong lĩnh vực “Thời trang nam thiết kế” với phong cách sáng tạo và thanh lịch.</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;">Với tầm nhìn chiến lược trở thành thương thiệu<strong> “Thời trang nam được yêu thích nhất Việt Nam”,</strong> từ những ngày đầu bước vào thị trường, Biluxury đã rất được chú trọng việc nghiên cứu, phát triển mẫu mã, phom dáng phủ hợp với hình thể đàn ông Việt. Nguồn nguyên liệu được kiểm soát chặt chẽ, máy móc và trang thiết bị hiện đại là đòn bẩy cho sự phát triển bền vững của thương hiệu.</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;">Đối tượng mà Biluxury hướng tới là nam giới trong độ tuổi từ 25 đến 35 tuổi, những người đàn ông thành đạt , luôn muốn có sự cân bằng giữa phát triển sự nghiệp và tận hưởng cuộc sống. Sản phẩm của Biluxury mang tới cho phái mạnh Việt hình ảnh trẻ trung, năng động, hiện đại mà không kém phần lịch sự, sang trọng. Được hàng trăm nghìn khách hàng thân thiết đặt niểm tin và lựa chọn là người bạn đồng hành mỗi ngày trong công việc và cuộc sống.</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;">Biluxury cam kết mang lại giá trị đích thực và một cuộc sống thịnh vượng hơn cho phái mạnh Việt</span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;"><img src="http://biluxury.vn/media/Tin%20tuc/showroom_1.jpg" alt="" width="100%"></span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p style="text-align: justify;"><span style="font-size: 16px;">Tháng 8/2017, Bimart thành lập học viện <strong>ĐÀO TẠO THỰC CHIẾN</strong> (daotaothucchien.com) - địa chỉ <strong>đầu tiên</strong> những kiến thức thực chiến kinh doanh thời trang từ chính CTHĐQT công ty - Lê Xuân Tùng. Các buổi học của ĐÀO TẠO THỰC CHIẾN tập trung vào trao đổi, chia sẻ thẳng thắn để tìm ra cách nhanh nhất, hiệu quả nhất giải quyết vấn đề thực tiễn của học viên. Mỗi khóa học ghi nhân sự tham gia của hàng trăm học viên là chủ shop, chủ doanh nghiệp, người đam mê thời trang. Tính đến cuối 2017, ĐÀO TẠO THỰC CHIẾN đã tổ chức được 5 lớp đào tạo, ghi nhân sự tham dự của trên 2.000 học viên</span></p>
				<p style="text-align: justify;"><span style="font-size: 16px;">&nbsp;</span></p>
				<p style="text-align: justify;"><span style="font-size: 16px;"><img src="https://scontent.fhan4-1.fna.fbcdn.net/v/t1.0-9/21557459_1661196367247523_4924076216355539827_n.jpg?oh=f0f1e85493c5660abaa74a8a1a83c4a1&amp;oe=5AC3D259" alt="Trong hình ảnh có thể có: 3 người, trong nhà" width="1900"></span></p>
				<p style="text-align: justify;">&nbsp;</p>
				<p><strong><span style="font-size: 22px;">MỐC THỜI GIAN</span></strong></p>
				<p>&nbsp;</p>
				<p><span style="font-size: 16px;"><strong>2009</strong> Ra đời Showroom thời trang Bionline đầu tiên</span></p>
				<p><span style="font-size: 16px;"><strong>2011</strong> Thành lập công ty TNHH thời trang Bionline</span></p>
				<p><span style="font-size: 16px;"><strong>2012</strong> Xây dựng hệ thống xưởng sản xuất chuyên nghiệp cho Bionline</span></p>
				<p><span style="font-size: 16px;"><strong>2014</strong> Ra đời thương hiệu Biluxury dành cho phân khúc thời trang nam cao cấp</span></p>
				<p><span style="font-size: 16px;"><strong>2015</strong> Phát triển hệ thống hơn 20 showroom, đại lý nhượng quyền trên toàn quốc</span></p>
				<p><span style="font-size: 16px;"><strong>2016</strong> &nbsp;Phát triển hệ thống &nbsp;50 showroom và đại lý nhượng quyền trên toàn quốc</span></p>
				<p><span style="font-size: 16px;"><strong>2017</strong> Mở rộng hệ thống hơn 75 showroom và đại lý nhượng quyền trên toàn quốc.</span></p>
				<p><span style="font-size: 16px;"><strong>8/2017</strong> Sáng lập học viện Đào Tạo Thực Chiến</span></p>
				<p><span style="font-size: 14px;"><span style="font-size: 16px;"><strong>2019</strong>&nbsp;Mục tiêu mở 300 showroom, trở thành thương hiệu thời trang nam hàng đầu Việt Nam</span>.</span></p></div>
				<div class="clb"><br></div>
				
				
				
			</div>
		</div>
	</div>
</div>
@endsection