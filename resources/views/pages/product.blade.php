@extends('layouts.app')
@section('content')
<div class="container-fluid product-page">
	<div class="breadcrumb-wrapper">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Pictures</a></li>
			<li><a href="#">Summer 15</a></li>
			<li>Italy</li>
		</ul>
	</div>
	<div class="row" >
		
		<div class="col-md-2 slider-left" id="gallery_09">
			<?php $array_item = explode(",", rtrim($product->image, ",")); ?>
			@foreach ($array_item as $i => $img)
			<?php
				$temp = str_replace("jpg","png",$img);
				$temp = str_replace("JPG","png",$temp);
			?>
			<a href="#" class="elevatezoom-gallery active" data-update="" data-image="{{ url('public/images/products-thum')}}/{{ $temp}}" data-zoom-image="{{ url('public/images/products')}}/{{$img}}">
				<img src="{{ url('public/images/products-thum')}}/{{ $temp }}">
			</a>
			@endforeach
			
		</div>
		<div class="col-md-5 zoom-img-center">
			<?php $temp0 = str_replace("JPG","png",$array_item[0]); ?>
			<img style="border:1px solid #e8e8e6;" id="zoom_09" src="{{ url('public/images/products')}}/{{ $array_item[0] }}" data-zoom-image="{{ url('public/images/products') }}/{{ $array_item[0] }}">
		</div>
		<div class="col-md-5 right-column-desc">
			<div class="name-product">{{ $product->name }}</div>
			<div class="code-product">{{ $product->link }}</div>
			<div class="price-product">{{ number_format($product->price, 0) }} đ</div>
			<input type="hidden" id="total_price" name="total_price" value="34534534">
			<div class="color-product" style="background: {{ App\Color::find($product->color)->code }};"></div>
			<div class="buy-product">
				<div class="buy-product-title">
					<div class="size-product">
						<div class="size-product-title">
							Kích cỡ
						</div>
						<div class="size-product-number ">
							<?php
								$array_size = explode(",", $product->size);
								$array_size = array_filter($array_size);
								foreach ($array_size as $size) {
										echo "<span style='border: 1px solid; border-radius: 50%; margin-right: 5px; padding: 1px 3px;'>". App\Size::find($size)->name ."</span>";
								}
							?>
						</div>
					</div>
					<div class="buy-tut">
						<button id="show_size_tutorial" type="button" class="btn btn-primary" data-toggle="modal" data-target="#tableSizeTutorial">
						Size tutorial
						</button>
						<button id="show_guide_buy" type="button" class="btn btn-primary" data-toggle="modal" data-target="#tableGuideToBuy">
						Guide To Buy
						</button>
					</div>
					<div class="quanity">
						<h3>Số Lượng</h3>
						<select id="quanity_list" name="quanity_select">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
					<input type="hidden" id="base_url" name="base_url" value="{{ url('/') }}">
					<button id="button_add_cart"  type="button" class="button-add" onclick="add_cart('{{ $product->id }}')"><i class="cart-in-menu fa fa-shopping-cart" aria-hidden="true"></i> add cart</button>
					<button id="button_click_buy" type="button" class="button-buy">mua hàng <span>(xem hàng tại nhà, không thích có thể trả lại)</span></button>
				</div>
			</div>
			<div class="share-button">
				<a href="#" title="Share this page on facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
				<a href="#" title="Share this page on twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href="#" title="Share this page on googleplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				<a href="#" title="Share this page on linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				<a href="#" title="Share this page on pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
			</div>
			<div class="describe-product">
				<div class="describe-product-title" data-toggle="collapse" data-target="#desc-product">
					<span>Mô Tả<i class="fa fa-caret-right" aria-hidden="true"></i></span>
				</div>
				<div id="desc-product" class="collapse">
					{{ $product->describe }}
				</div>
			</div>
			
			<div class="material-product">
				<div class="material-product-title" data-toggle="collapse" data-target="#mat-product">
					<span>Chất liệu và sử dụng <i class="fa fa-caret-right" aria-hidden="true"></i></span>
				</div>
				<div id="mat-product" class="collapse">
					{{ $product->material }}
				</div>
			</div>
		</div>
	</div>
	<hr>
	<?php echo "<pre>"; var_dump($_COOKIE); ?>
	<div class="row seen-detail-products">
		<p> Sản Phẩm Đã Xem</p>
		<div class="owl-carousel owl-theme list-product-viewed">
			@foreach ($product_viewed as $key => $itemA)
			<?php
				$array = explode(",", $itemA->image);
				$array = array_filter($array);
			?>
			<div class="item">
				<a href="{{ url('/product') }}/{{ $itemA->link }}" title="">
					<img style="width: 100%;" src="{{ url('public/images/products') }}/{{ $array[0] }}" alt="">
				</a>
				<div class="caption-in-viewed">
					<h3>
					fgfgfgfg
					</h3>
					
				</div>
			</div>
			
			@endforeach
		</div>
	</div>
</div>
@include('partial.modal')
@endsection