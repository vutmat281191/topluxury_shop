@extends('layouts.app')

@section('content')

	@if ($count === 0)
		<div class="banner-static-pages">
			<h1 class="title-static-page">Tìm kiếm</h1>
			<p class="desc-static-page">Campaign not found.</p>
		</div>
	@else
		<div class="container-fluid search-campain-content">		
			<div class="row search-campain-content">
				<div class="col-md-12 filter-part" style="display: flex; justify-content: flex-end;">
				    <div class="dropdown">
				      	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter</button>
				      	<ul class="dropdown-menu">
					       	<?php
					        if (strpos(url()->full(), '?')) {
					         $url_price_asc = url()->full() . '&price=asc';
					         $url_price_desc = url()->full() . '&price=desc';
					         $url_create_desc = url()->full() . '&created_at=desc';
					         $url_filter_grid = url()->full() . '&grid=';
					        }
					        else {
					         $url_price_asc = url()->full() . '?price=asc';
					         $url_price_desc = url()->full() . '?price=desc';
					         $url_create_desc = url()->full() . '?created_at=desc';
					         $url_filter_grid = url()->full() . '?grid=';
					        }
					       ?>
					       <li><a href="{{ $url_create_desc }}">Newest</a></li>
					       <li><a href="{{ $url_price_asc }}">Price increase</a></li>
					       <li><a href="{{ $url_price_desc }}">Price discrease</a></li>
				      	</ul>
				    </div>
				    <div class="filter-grid">
				      <a href="{{ $url_filter_grid }}3">
				       <img src="{{ url('/public/images/grid-3.png') }}" alt="">
				      </a>
				      <a href="{{ $url_filter_grid }}5">
				       <img src="{{ url('/public/images/grid-5.png') }}" alt="">
				      </a>
				    </div>
			    </div>
				@foreach ($list as $item)
				<div class="{{ $class_grid }} item_product_list" style="max-width: none;">
					<div class="border">
						<span class="icon_new"></span><span><span class="discount"></span></span>
						<div class="img">
							<a href="{{ url('/product') }}/{{ $item->link }}" title="{{ $item->name }}">

								<?php $array_item = explode(",", rtrim($item->image, ",")); ?>
								<img src="{{ url('/public/images/products') }}/{{ $array_item[0] }}" width="387" title="{{ $item->name }}" alt="{{ $item->name }}" class="first" style="opacity: 1;">
								<img src="{{ url('/public/images/products') }}/{{ $array_item[1] }}" width="387" title="{{ $item->name }}" alt="{{ $item->name }}" class="second" style="opacity: 0;">
							</a>
							<span class="loader">&nbsp;</span>
						</div>
						<div class="info">
						    <h3 class="clb"><a href="{{ url('/product') }}/{{ $item->link }}" title="{{ $item->name }}">{{ $item->name }}</a></h3>
						    <div class="brief clearfix">
						    	<p><span class="price-old"></span> {{ $item->price }} đ</p>
						    </div>
						    <div><span class="color-dot" style=" background: {{ App\Color::find($item->color)->code }}"></span></div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	@endif
@endsection
