@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<form name="frm_shopping_cart" method="post" action="" style="width: 100%;">
			<div class="table">
				<?php //echo "<pre>";var_dump($product_cart);exit(); ?>
				<table width="100%" border="0" cellspacing="1" cellpadding="1" class="cart_list">        
					<thead>
						<tr>
							<th>Sản phẩm</th>
							<th>Số lượng</th>
							<th>Đơn giá</th>
							<th>Thành tiền</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($product_cart as $product)
							<?php
								// echo "<pre>";
								// var_dump($product);
							?>
							<tr>
								<td>
									<?php
										$array = explode(",", $product->image);
										$array = array_filter($array);
									?>
									<a class="del" onclick="delete_item_cart({{ $product->id }})" title="Xóa" style="float: left; padding: 0 10px; cursor: pointer; font-size: 18px;">
										<i class="fa fa-times" aria-hidden="true"></i>
									</a>
									<span class="img-in-cart" style="float: left;">
										<a href="{{ url('/product') }}/{{ $product->link }}" title="Polo xanh tím than">
											<img src="{{ url('public/images/products') }}/{{ $array[0] }}" width="60" border="0" title="Polo xanh tím than" alt="Polo xanh tím than">
										</a>
									</span>
									<span class="detail-in-cart" style="float: left; padding-left: 10px; text-align: left;">
										<span style="display: block;">{{ $product->name }}</span>
										<span style="display: block;">Mã: {{ $product->link }}</span>
										<span style="display: block;">Màu sắc: {{ App\Color::find($product->color)->name }}</span>                
										<span style="display: block;">Kích cỡ: 40</span>                
									</span>
								</td>
								<td class="quanity-in-cart-td">
									<div style="width:90px;">
										<input type="text" class="form-control" id="quanity-in-cart" name="quanity-in-cart" value="1">
										<input type="hidden" class="total-price-hidden" name="total-price-hidden" value="0">
									</div>
								</td>
								<input type="hidden" id="price-in-cart" name="price-in-cart" value="{{ $product->price }}">
								<td><span>{{ $product->price }}</span></td>
								<td class="total-price-td"><span class="total-price">{{ $product->price }}</span></td>		    
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="sum-wrapper" style="margin: 13px 0;">
				<div class="sum-inner" style="float: right; font-size: 15px; font-weight: bold;">
					<span class="total-text">Total: </span>
					<input type="hidden" id="total_price" name="total_price" value="34534534">
					<span class="res-total">34535345</span>
				</div>
			</div>
			<div class="col-md-12 button-wrapper" style="margin: 10px 0 0 0; padding: 0; border-top: 1px solid #181818;">
				<a href="{{ url('/') }}" class="button-black" style="float: left;">Tiếp tục mua hàng</a>
				<button id="button_buy_in_cart" class="button-black" style="float: right;">Thanh Toán</button>
			</div>
		</form>
	</div>
</div>
@include('partial.modal')
@endsection