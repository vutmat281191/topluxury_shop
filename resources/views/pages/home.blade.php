@extends('layouts.app')

@section('content')
<div class="container-fluid custome-container">
	<div class="home-page">
		<div id="myCarousel" class="carousel slide" data-ride="carousel"><!-- Indicators -->		
			<!-- Wrapper for slides -->
			<div class="animated fadeIn carousel-inner">
				<div class="item ">
					<img src="{{ url('public/images/slider-home') }}/slide5.png" alt="New york" style="width: 100%;">
				</div>
				<div class="item active">
					<img src="{{ url('public/images/slider-home') }}/slide6.jpg" alt="New york" style="width: 100%;">
				</div>
				<div class="item ">
					<img src="{{ url('public/images/slider-home') }}/slide1.jpg" alt="Los Angeles" style="width: 100%;">
				</div>
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<i class="fa fa-angle-left" aria-hidden="true"></i>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<i class="fa fa-angle-right" aria-hidden="true"></i>
			</a>
		</div>		
			<div class="newest-produces">
				<h3 class="line-products">New Products</h3>
				<div class = "content-2-column" >
					<div class=" animated zoomIn  after-slider">
						<div class="item-products">
							<a href="{{url('category/f0de8ef97a1de9b82be166f35fb33f55') }}">
								<img class = ""  src="public/images/products/Home/1.jpg">
								<div class="overlay">
									<div class="content">
											<h2>POLO NĂNG ĐỘNG</h2>
											<span>mua ngay</span>
									</div>	
								</div>
							</a>
						</div>
						<div class=" item-products">
							<a href="{{url('category/ab25e9127a3f920d9643d5fd758dbf44') }}">
								<img class = ""  src="public/images/products/Home/2.jpg">
								<div class="overlay">
									<div class="content">
									<h2>SƠ MI TRẺ TRUNG</h2>
										<span> mua ngay</span>
									</div>	
								</div>
							</a>
						
						</div>
					</div>
				</div>
				<div class = "content-3-column" >
					<div class=" animated zoomIn before-footer" >
						<div class="item-products">
							<a href="{{url('category/ae06cbb86480d1322a1021822617f025') }}">
								<img class = ""  src="public/images/products/Home/3.jpg">
								<div class="overlay">
									<div class="content">
									<h2>ÁO PHÔNG CỔ TRÒN</h2>
											<span> mua ngay</span>
									</div>	
								</div>
									
							</a>
						</div>
						<div class="item-products">
							<a href="{{url('category/ae06cbb86480d1322a1021822617f025') }}">
								<img class = ""  src="public/images/products/Home/4.jpg">
								<div class="overlay">
									<div class="content">
										<h2>hợp tác với luxury</h2>
										<span> mua ngay</span>
									</div>	
								</div>					
							</a>
						</div>
						<div class="item-products">
							<a href="{{url('category/ae06cbb86480d1322a1021822617f025') }}">
								<img class = "lazy"  src="public/images/products/Home/5.jpg">
								<div class="overlay">
									<div class="content">
									<h2>QUẦN ÂU THANH LỊCH</h2>
										<span> mua ngay</span>
									</div>	
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		
		<div class="register-information">
			<h4 class="end-line-products">Register to get information</h4>
		</div>
		<div class="banner-footer"> 
			<img src="public/images/products/Home/done.jpg" alt="bannerfooter">
			<div class="form-newsletter-wrapper">
				{{ Form::open(['url' => 'submit_newsletter','class' =>'form-newsletter']) }}
			      	{!! Form::token() !!}
			        {!! Form::email('newsletter', null, ['placeholder' => 'Your email', 'class' => 'newsletter-bar']) !!}
					<button type="submit">SEND</button>
			    {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection