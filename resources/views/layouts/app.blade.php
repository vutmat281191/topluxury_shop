<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Biluxury') }}</title>
	<link rel="shortcut icon" href="{{ url('public/favicon.ico') }}" type="image/ico">

	<!-- Styles -->
	<link href="{{ url('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/custome.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ url('public/css/animate.css') }}" rel="stylesheet">
	<link href="{{url('public/vendor/OwlCarousel2/dist/assets/owl.carousel.min.css') }}" rel="stylesheet" >
	<link href="{{url('public/vendor/OwlCarousel2/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet" >

</head>
<body>
	<div id="app" >
		<nav class="navbar navbar-default navbar-static-top ">
			<div class="main-jumbo top-banner-contact">
				<div class="s-top" >
					<div class="container-fluid info-top" >
						<div id="s-hotline" class="s-mod">						
								<ul class="hidden-xs">
									<!-- <li>
										<i class="fa fa-truck" aria-hidden="true"></i>
										<a href="#">MIỄN PHÍ VẬN CHUYỂN</a>
									</li>
									<li>
										<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
										<a href="#">THANH TOÁN KHI NHẬN HÀNG</a>
									</li> -->
									<li>
										<i class="fa fa-retweet" aria-hidden="true"></i>
										<a href="#">ĐỔI TRẢ HÀNG TRONG 7 NGÀY</a>
									</li>							
									<li class="phone-mobile-only">
										<span >
											<span class="hotline-banner-contact">HOTLINE:</span>
											<a href="tel: 0449646238" onclick="ga('send', 'event', 'Click Gọi', 'Phone', 'Header');">0449646238</a> Or
											<a href="tel: 0449646238" onclick="ga('send', 'event', 'Click Gọi', 'Phone', 'Header');">(+61)449646238</a>
										</span>
									</li>
								</ul>						
						</div>
						
						<!-- <div id="s-user" class="s-mod">
							<div id="s-user-popup" class="user-popup" >	
								
									<div class="hidden-xs">
										<a href="http://biluxury.vn/checkout" class="s-cart"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>Giỏ hàng <span class="icon"></span>
											<span class="num"></span></a>
									
										<a href="#" class="s-login"><span class="hi"> <i class="fa fa-user" aria-hidden="true"></i>Đăng nhập</span></a>
										<a href="#" class="s-register"><span class="hi">&nbsp;&nbsp;|&nbsp;&nbsp;Đăng ký</span></a>		
										</div>
							</div>
						</div> -->
					</div>
				</div>
			
				<div class="logo-page">
					<a href="{{ url('/') }}"><img src="{{url('public/images/logo-biluxury.png') }}" alt="logohere"></a>
				</div>
			</div>
				<!-- <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{ url('/') }}">
						{{ config('app.name', 'GT project') }}
					</a>
				</div>

				<div class="collapse navbar-collapse" id="app-navbar-collapse">
				</div> -->
				@include('partial.menu')
			</div>
		</nav>
		<div id="fb-root"></div>
		@yield('content')
		<div class="footer">
			<div class="menu-in-footer">
				<a href="{{ url('#') }}">Policy </a><span class="right-line-footer">|</span>
				<a href="{{ url('category/f0de8ef97a1de9b82be166f35fb33f10') }}">Promotion</a><span class=" right-line-footer">|</span>
				<a href="{{ url('contact-us') }}">Contact Us</a><span class=" right-line-footer">|</span>
				<a href="{{ url('category/f0de8ef97a1de9b82be166f35fb33f17') }}">About Us</a>
			</div>
			<div class="boder-for-menu"></div>
			<div class="social-icon-in-footer">
				<h3>Follow us</h3>
				<a href=""> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
				<a href=""> <i class="fa fa-youtube" aria-hidden="true"></i></a>
				<a href=""> <i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href=""> <i class="fa fa-google-plus" aria-hidden="true"></i></a>
				<a href=""> <i class="fa fa-pinterest" aria-hidden="true"></i></a>
				<a href="">	<i class="fa fa-twitter" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<!-- <script src="{{ url('public/js/app.js') }}"></script> -->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="{{url('public/js/app.js') }}"></script>
	<script src="{{url('public/js/custome.js') }}"></script>
	<script src="{{url('public/js/bootstrap.min.js') }}"></script>
	<script src="{{url('public/js/tai_custome.js') }}"></script>
	<script src="{{url('public/js/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
	<script src="{{url('public/js/vendor/tinymce/tinymce.min.js') }}"></script>
	<script src="{{url('public/js/vendor/elevatezoom/jquery.elevatezoom.js') }}"></script>
	<script src="{{url('public/vendor/OwlCarousel2/dist/owl.carousel.min.js') }}"></script>
	<script src="{{url('public/js/vendor/jquery-cookie.js') }}"></script>
</body>
</html>
